package server.test;

import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;
import com.jcraft.jsch.JSchException;
import javafx.util.Pair;
import server.cache.CacheManager;
import server.client.JobRequest;
import server.comm.SSHConnection;
import server.comm.SSHUserInfo;
import server.policy.job.OldestJobSelectionPolicy;
import server.policy.vm.FirstAvailableVMPolicy;
import server.resource.ResourceManager;
import server.resource.VMInstance;
import server.resource.VMState;
import server.schedule.Job;
import server.schedule.JobManager;
import server.schedule.Scheduler;
import server.stats.Analyzer;
import server.util.CloudLogger;
import server.util.EC2ClientAccessor;
import server.util.StaticProperties;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * This class will be used to run tests which will be analyzed and presented in the paper
 *
 * @author Dan Graur 11/1/2017
 */
public class Benchmark {

    public static void main(String[] args) throws IOException, JSchException {

        /* First read the test file */
        Scanner reader = new Scanner(new File(StaticProperties.CONFIGURATION_DIR + StaticProperties.TEST_CONFIGURATION));

        /* Then build the tests */
        List<Pair<String, Long>> individualTests = new ArrayList<>();

        while (reader.hasNextLine()) {
            String[] tokens = reader.nextLine().split(",");

            individualTests.add(new Pair<>(tokens[0], Long.parseLong(tokens[1])));
        }



        long cacheTime = System.currentTimeMillis();

        CacheManager cm = CacheManager.getInstance();

        cacheTime = System.currentTimeMillis() - cacheTime;

        try {
            Thread.sleep(30 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ResourceManager rm = ResourceManager.instance();
        JobManager jm = JobManager.instance();

        /* Create the Scheduler thread, and the Resource Manger thread*/
        Scheduler scheduler = new Scheduler(
                new OldestJobSelectionPolicy(),
                new FirstAvailableVMPolicy()
        );

        Thread cacheManager = new Thread(cm);
        Thread schedulerThread = new Thread(scheduler);
        Thread resourceManager = new Thread(rm);
        Thread jobManager = new Thread(jm);

        /* Start the Cache Manager */
        cacheManager.start();

        /* Start up the resource manager. */
        resourceManager.start();

        /* Start the Scheduler */
        schedulerThread.start();

        /* Start the Job Manager thread */
        jobManager.start();

        boolean useAlreadyExisting = false;

        if (useAlreadyExisting) {

            DescribeInstancesResult result = EC2ClientAccessor.getInstance().describeInstances(
                    new DescribeInstancesRequest()
            );

            VMInstance addedInstance = null;

            for (Reservation reservation : result.getReservations()) {
                for (Instance instance : reservation.getInstances()) {
                    addedInstance = new VMInstance(
                            instance.getInstanceId(),
                            instance.getPublicIpAddress(),
                            instance
                    );

                    System.out.println(
                            String.format(
                                    "Found instance: %s, having IP %s, with state: %s",
                                    instance.getInstanceId(),
                                    instance.getPublicIpAddress(),
                                    instance.getState().getName()
                            )
                    );

                /* If the instance currently selected is not running skip it */
                    if (!VMState.integerToState(instance.getState().getCode()).equals(VMState.RUNNING))
                        continue;

                    SSHUserInfo userInfo = new SSHUserInfo(
                            StaticProperties.VM_USERNAME,
                            StaticProperties.TRUST_ENTITIES,
                            StaticProperties.KEYPAIR_LOCAITON
                    );

                    boolean connected = false;

                    for (int i = 1; i <= 5 && !connected; ++i) {
                        try {
                            addedInstance.setAndOpenVmConnection(
                                    new SSHConnection(
                                            userInfo,
                                            addedInstance.getIpAddress()
                                    )
                            );

                            connected = true;
                        } catch (JSchException e) {
                            if (i == 5)
                                throw e;

                        }
                    }

                    addedInstance.markStartTime();
                    rm.addVM(addedInstance);
                }
            }
        }

        long sTime = System.currentTimeMillis() + cacheTime;

        /* Job submitting loop */
        for (int i = 0; i < StaticProperties.TOTAL_JOBS; i++) {

            String request = individualTests.get(i).getKey();
            long sleepTime = individualTests.get(i).getValue();


            System.err.printf(
                    "Creating and submitting a new job %s, with waiting time %d\n",
                    request,
                    sleepTime
            );

            jm.addPendingJob(
                    new Job(
                            Job.generateJobID(),
                            new JobRequest(
                                    request,
                                    i
                             )
                    )
            );


            try {
                Thread.sleep(sleepTime * 1000);
            } catch (InterruptedException e) {
                System.err.println("Was interrupted whilst I was sleeping: " + e.getMessage());

                e.printStackTrace();
            }
        }


        /* Do not kill the application yet */
        System.in.read();

        long overallTime = System.currentTimeMillis() - sTime;

        for (int i =0; i < 10; ++i)
            CloudLogger.getInstance().info(
                    "The application's makespan: " + overallTime
            );




            /* Can record the analysis information */
        rm.updateStats();
        Analyzer.getInstance().summarize();

        CloudLogger.getInstance().info(
                "Application is starting the shutdown procedure"
        );

        CloudLogger.getInstance().info(
                "Main application threads will be interrupted"
        );

            /* Interrupt the application's main threads */
            /*
            schedulerThread.interrupt();
            jobManager.interrupt();
            resourceManager.interrupt();
            */
        cm.setInterrupt(true);
        rm.setInterrupt(true);
        jm.setInterrupt(true);
        scheduler.setInterrupt(true);


        CloudLogger.getInstance().info(
                "Application threads have been interrupted. Waiting for them to gracefully terminate."
        );

        CloudLogger.getInstance().info(
                "Waiting on Scheduler to finish."
        );
        try {
            schedulerThread.join();
        } catch (InterruptedException e) {
            CloudLogger.getInstance().error("Interrupted while waiting for Scheduler.", e);
        }

        CloudLogger.getInstance().info(
                "Waiting on Job Manager to finish."
        );
        try {
            jobManager.join();
        } catch (InterruptedException e) {
            CloudLogger.getInstance().error("Interrupted while waiting for Job Manager.", e);
        }

        CloudLogger.getInstance().info(
                "Waiting on Resource Manager to finish."
        );
        try {
            resourceManager.join();
        } catch (InterruptedException e) {
            CloudLogger.getInstance().error("Interrupted while waiting for Resource Manager.", e);
        }

        CloudLogger.getInstance().info(
                "All threads have been successfully terminated. The application has had a graceful termination."
        );


    }

}
