package server.test;

import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;
import com.jcraft.jsch.JSchException;
import server.comm.SSHConnection;
import server.comm.SSHConnectionException;
import server.comm.SSHUserInfo;
import server.comm.utility.CommandCreator;
import server.resource.VMInstance;
import server.resource.VMState;
import server.stats.MeasurementResult;
import server.util.EC2ClientAccessor;
import server.util.StaticProperties;

import java.io.IOException;

/**
 * @author Dan Graur 11/1/2017
 */
public class Admin {

    public static void main(String[] args) throws JSchException, IOException, SSHConnectionException {

        DescribeInstancesResult result = EC2ClientAccessor.getInstance().describeInstances(
                new DescribeInstancesRequest()
        );

        VMInstance addedInstance = null;

        for (Reservation reservation : result.getReservations()) {
            for (Instance instance : reservation.getInstances()) {
                addedInstance = new VMInstance(
                        instance.getInstanceId(),
                        instance.getPublicIpAddress(),
                        instance
                );

                System.out.println(
                        String.format(
                                "Found instance: %s, having IP %s, with state: %s",
                                instance.getInstanceId(),
                                instance.getPublicIpAddress(),
                                instance.getState().getName()
                        )
                );

                /* If the instance currently selected is not running skip it */
                if (!VMState.integerToState(instance.getState().getCode()).equals(VMState.RUNNING))
                    continue;

                SSHUserInfo userInfo = new SSHUserInfo(
                        StaticProperties.VM_USERNAME,
                        StaticProperties.TRUST_ENTITIES,
                        StaticProperties.KEYPAIR_LOCAITON
                );

                boolean connected = false;

                for (int i = 1; i <= 5 && !connected; ++i) {
                    try {
                        addedInstance.setAndOpenVmConnection(
                                new SSHConnection(
                                        userInfo,
                                        addedInstance.getIpAddress()
                                )
                        );

                        connected = true;
                    } catch (JSchException e) {
                        if (i == 5)
                            throw e;

                    }
                }

            }
        }

        /* Testing the upload time */

        String[] fileName = {"video1"};
        System.out.println("Starting testing");

        for (String aFileName : fileName) {

            int runs = 20;

            MeasurementResult upload = new MeasurementResult("Upload-" + aFileName);
            MeasurementResult conversion = new MeasurementResult("Conversion-" + aFileName);
            MeasurementResult download = new MeasurementResult("Download-" + aFileName);

            for (int i = 0; i < runs; ++i) {

                long currentTime = System.currentTimeMillis();

                addedInstance.getVmConnection().uploadFile(
                        "C:\\DAN_PC\\Facultate\\Master\\Cloud Computing\\RoialtyFreeVideos\\For_Testing\\" + aFileName + ".mp4",
                        "/home/videos/" + aFileName + "/" + aFileName + i + ".mp4"
                );

                upload.addMeasurement(System.currentTimeMillis() - currentTime);

                currentTime = System.currentTimeMillis();

                addedInstance.getVmConnection().executeCommand(
                        CommandCreator.ffmpegFromMP4toAVI(
                                "/home/videos/" + aFileName + "/",
                                aFileName + i + ".mp4",
                                aFileName + i + ".avi"
                        )
                );

                conversion.addMeasurement(System.currentTimeMillis() - currentTime);

                currentTime = System.currentTimeMillis();

                addedInstance.getVmConnection().downloadFile(
                                "C:\\DAN_PC\\Facultate\\Master\\Cloud Computing\\RoialtyFreeVideos\\For_Testing\\Download\\" + aFileName + i + ".avi",
                                "/home/videos/" + aFileName + "/" + aFileName + i + ".avi"
                );

                download.addMeasurement(System.currentTimeMillis() - currentTime);
            }

            System.out.println(
                    String.format(
                            "The pverall performance of the experiment on %s is:\n" +
                                    " - Upload: %s\n" +
                                    " - Conversion: %s\n" +
                                    " - Download: %s\n",
                            aFileName,
                            upload.toString(),
                            conversion.toString(),
                            download.toString()
                    )
            );

            MeasurementResult.writeToCSV(
                    "C:\\DAN_PC\\Facultate\\Master\\Cloud Computing\\RoialtyFreeVideos\\For_Testing\\Results\\",
                    aFileName,
                    upload,
                    conversion,
                    download
            );
        }

        System.out.println("Ending testing");

    }
}
