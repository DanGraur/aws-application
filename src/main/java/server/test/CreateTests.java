package server.test;

import server.util.StaticProperties;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * @author Dan Graur 11/1/2017
 */
public class CreateTests {

    private static Random random = new Random();

    private static double getFromExponentialDistribution(double mean) {
        return Math.log(1 - random.nextDouble()) / -mean;
    }

    public static void main(String[] args) throws IOException {


        String[] files = {"861023712", "A0450_1507_H3_807_1163_Videvo", "POINTSETTA_4K"};
        int sampleNumber = 40;

        List<String> selectedInputs = new ArrayList<>();
        File videoNameFile = new File(StaticProperties.CONFIGURATION_DIR + "videos.txt");
        PrintWriter out = new PrintWriter(videoNameFile);


        /* Create the contents of the test */
        for (int i = 0; i < sampleNumber; ++i) {
            String file = files[random.nextInt(3)];

            selectedInputs.add(file);
            out.println(file);
        }

        out.flush();
        out.close();

        Map<Integer, Double> intervals = new HashMap<>();

        for (int mean = 5; mean <= 5; mean += 5) {
            File experimentFile = new File(StaticProperties.CONFIGURATION_DIR + "new_exponential_" + mean + ".txt");
            experimentFile.createNewFile();
            PrintWriter experimentOut = new PrintWriter(experimentFile);

            for (int i = 0; i < sampleNumber; ++i)
                experimentOut.println("POINTSETTA_4K" + "," + (long) getFromExponentialDistribution(1.0 / mean));
                //experimentOut.println(selectedInputs.get(i) + "," + (long) getFromExponentialDistribution(1.0 / mean));

            experimentOut.flush();
            experimentOut.close();
        }


    }

}
