package server.test;

import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;
import com.jcraft.jsch.JSchException;
import server.client.JobRequest;
import server.comm.SSHConnection;
import server.comm.SSHUserInfo;
import server.policy.job.OldestJobSelectionPolicy;
import server.policy.vm.FirstAvailableVMPolicy;
import server.resource.ResourceManager;
import server.resource.VMInstance;
import server.resource.VMState;
import server.schedule.Job;
import server.schedule.JobManager;
import server.schedule.Scheduler;
import server.util.CloudLogger;
import server.util.EC2ClientAccessor;
import server.util.StaticProperties;

import java.io.IOException;

/**
 * @author Dan Graur 10/30/2017
 */
public class AlreadyInitiatedClass {

    public static void main(String[] args) throws JSchException {
        /* Declarations area */
        int jobId = 0;
        ResourceManager resourceManager = ResourceManager.instance();
        Scheduler scheduler = new Scheduler(
                new OldestJobSelectionPolicy(),
                new FirstAvailableVMPolicy()
        );
        JobManager jobManager = JobManager.instance();

        DescribeInstancesResult result = EC2ClientAccessor.getInstance().describeInstances(
                new DescribeInstancesRequest()
        );

        Thread rmThread = new Thread(resourceManager);
        Thread sThread = new Thread(scheduler);
        Thread jmThread = new Thread(jobManager);

        rmThread.start();
        sThread.start();
        jmThread.start();

        for (Reservation reservation : result.getReservations()) {
            for (Instance instance : reservation.getInstances()) {
                VMInstance addedInstance = new VMInstance(
                        instance.getInstanceId(),
                        instance.getPublicIpAddress(),
                        instance
                );

                System.out.println(
                        String.format(
                                "Found instance: %s, having IP %s, with state: %s",
                                instance.getInstanceId(),
                                instance.getPublicIpAddress(),
                                instance.getState().getName()
                        )
                );

                /* If the instance currently selected is not running skip it */
                if (!VMState.integerToState(instance.getState().getCode()).equals(VMState.RUNNING))
                    continue;

                SSHUserInfo userInfo = new SSHUserInfo(
                        StaticProperties.VM_USERNAME,
                        StaticProperties.TRUST_ENTITIES,
                        StaticProperties.KEYPAIR_LOCAITON
                );

                boolean connected = false;

                for (int i = 1; i <= 5 && !connected; ++i) {
                    try {
                        addedInstance.setAndOpenVmConnection(
                                new SSHConnection(
                                        userInfo,
                                        addedInstance.getIpAddress()
                                )
                        );

                        connected = true;
                    } catch (JSchException e) {
                        if (i == 5)
                            throw e;

                    }
                }

                resourceManager.addVM(addedInstance);
            }
        }

         /* Register a shutdown hook */
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            CloudLogger.getInstance().info(
                    "Application is starting the shutdown procedure"
            );

            CloudLogger.getInstance().info(
                    "Main application threads will be interrupted"
            );
            /* Interrupt the application's main threads */
            sThread.interrupt();
            jmThread.interrupt();
            rmThread.interrupt();


            CloudLogger.getInstance().info(
                    "Application threads have been interrupted. Waiting for them to gracefully terminate."
            );

            CloudLogger.getInstance().info(
                    "Waiting on Scheduler to finish."
            );
            try {
                sThread.join();
            } catch (InterruptedException e) {
                CloudLogger.getInstance().error("Interrupted while waiting for Scheduler.", e);
            }

            CloudLogger.getInstance().info(
                    "Waiting on Job Manager to finish."
            );
            try {
                jmThread.join();
            } catch (InterruptedException e) {
                CloudLogger.getInstance().error("Interrupted while waiting for Job Manager.", e);
            }

            CloudLogger.getInstance().info(
                    "Waiting on Resource Manager to finish."
            );
            try {
                rmThread.join();
            } catch (InterruptedException e) {
                CloudLogger.getInstance().error("Interrupted while waiting for Resource Manager.", e);
            }

            CloudLogger.getInstance().info(
                    "All threads have been successfully terminated. The application has had a graceful termination."
            );

        }));

        while(true) {

            System.err.println("Creating and submitting a new job");

            JobManager.instance().addPendingJob(
                    new Job(
                            Job.generateJobID(),
                            new JobRequest(
                                    "861023712",
                                    jobId++
                            )
                    )
            );

            JobManager.instance().addPendingJob(
                    new Job(
                            Job.generateJobID(),
                            new JobRequest(
                                    "861023712",
                                    jobId++
                            )
                    )
            );

            System.err.println("Job has been submitted. Awaiting key-press to submit another job");

            try {
                System.in.read();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
