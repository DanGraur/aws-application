package server.test;

import server.client.JobRequest;
import server.policy.job.OldestJobSelectionPolicy;
import server.policy.vm.FirstAvailableVMPolicy;
import server.resource.ResourceManager;
import server.schedule.Job;
import server.schedule.JobManager;
import server.schedule.Scheduler;
import server.util.CloudLogger;

import java.io.IOException;

/**
 * @author Dan Graur 10/30/2017
 */
public class TesterClass {

    public static void main(String[] args) {

        ResourceManager rm = ResourceManager.instance();
        JobManager jm = JobManager.instance();

        /* Create the Scheduler thread, and the Resource Manger thread*/
        Scheduler scheduler = new Scheduler(
                new OldestJobSelectionPolicy(),
                new FirstAvailableVMPolicy()
        );

        Thread schedulerThread = new Thread(scheduler);
        Thread resourceManager = new Thread(rm);
        Thread jobManager = new Thread(jm);
        Thread mainThread = Thread.currentThread();

        /* Start up the resource manager (this should typically be the first thing that starts). */
        resourceManager.start();

        /* Start the Scheduler */
        schedulerThread.start();

        /* Start the Job Manager thread */
        jobManager.start();

        int id = 0;

        /* Register a shutdown hook */
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            CloudLogger.getInstance().info(
                    "Application is starting the shutdown procedure"
            );

            CloudLogger.getInstance().info(
                    "Main application threads will be interrupted"
            );

            /* Interrupt the application's main threads */
            /*
            schedulerThread.interrupt();
            jobManager.interrupt();
            resourceManager.interrupt();
            */
            rm.setInterrupt(true);
            jm.setInterrupt(true);
            scheduler.setInterrupt(true);


            CloudLogger.getInstance().info(
                    "Application threads have been interrupted. Waiting for them to gracefully terminate."
            );

            CloudLogger.getInstance().info(
                    "Waiting on Scheduler to finish."
            );
            try {
                schedulerThread.join();
            } catch (InterruptedException e) {
                CloudLogger.getInstance().error("Interrupted while waiting for Scheduler.", e);
            }

            CloudLogger.getInstance().info(
                    "Waiting on Job Manager to finish."
            );
            try {
                jobManager.join();
            } catch (InterruptedException e) {
                CloudLogger.getInstance().error("Interrupted while waiting for Job Manager.", e);
            }

            CloudLogger.getInstance().info(
                    "Waiting on Resource Manager to finish."
            );
            try {
                resourceManager.join();
            } catch (InterruptedException e) {
                CloudLogger.getInstance().error("Interrupted while waiting for Resource Manager.", e);
            }

            CloudLogger.getInstance().info(
                    "Waiting on the main thread to finish."
            );
            try {
                mainThread.join();
            } catch (InterruptedException e) {
                CloudLogger.getInstance().error("Interrupted while waiting for Main Thread.", e);
            }

            CloudLogger.getInstance().info(
                    "All threads have been successfully terminated. The application has had a graceful termination."
            );

        }));


        while (true) {

            System.err.println("Creating and submitting a new job");

            JobManager.instance().addPendingJob(
                    new Job(
                            Job.generateJobID(),
                            new JobRequest(
                                    "861023712",
                                    id++
                            )
                    )
            );

            System.err.println("Job has been submitted. Awaiting key-press to submit another job");

            try {
                System.in.read();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }


    }
}
