package server.cache;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import server.util.StaticProperties;

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

/**
 * @author Dan Graur 11/4/2017
 */
public class CacheManager implements Runnable {

    private static CacheManager instance;

    private final Map<String, Long> cacheInfo;
    private AmazonS3 s3;
    private static final String BUCKET_NAME = "app-cache-bucket" + UUID.randomUUID();;
    private boolean interrupt;

    /**
     * The timer which controls the amount of time an element can exist in cache. 5 minutes currently
     */
    private static final long OBJECT_TIMEOUT = 15 * 60 * 1000;


    private CacheManager() {
        interrupt = false;

        cacheInfo = new HashMap<>();
        s3 = new AmazonS3Client(new BasicAWSCredentials(
                StaticProperties.ACCESS_KEY,
                StaticProperties.PRIVATE_ACCESS_KEY
        )
        );

        s3.setEndpoint(Region.getRegion(Regions.US_EAST_1).getServiceEndpoint("s3"));

        s3.createBucket(BUCKET_NAME);

        System.out.println("The application has added the file to the S3 storage: " +
                addFileIfNotExists(
                        "1234",
                        new File(
                                "C:\\DAN_PC\\Facultate\\Master\\Cloud Computing\\RoialtyFreeVideos\\For_Testing\\Download\\POINTSETTA_4K0" + StaticProperties.TARGET_TYPE
                        )
                )
        );
    }

    public static CacheManager getInstance() {
        if (instance == null)
            instance = new CacheManager();

        return instance;
    }

    public void setInterrupt(boolean interrupt) {
        this.interrupt = interrupt;
    }


    public void run() {

        while(!interrupt) {

            synchronized (cacheInfo) {
                long removeBelow = System.currentTimeMillis() - OBJECT_TIMEOUT;

                Iterator<Map.Entry<String, Long>> iterator = cacheInfo.entrySet().iterator();

                while (iterator.hasNext()) {
                    Map.Entry<String, Long> current = iterator.next();

                    if (current.getValue() < removeBelow) {
                        iterator.remove();

                        s3.deleteObject(new DeleteObjectRequest(BUCKET_NAME, current.getKey()));
                    }
                }

                cacheInfo.notifyAll();
            }

            try {
                Thread.sleep(OBJECT_TIMEOUT);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        terminateCache();
    }

    public String getFileIfExists(String request) throws IOException {

        String pathToFile = null;

        synchronized (cacheInfo) {

            if (cacheInfo.containsKey(request)) {
                cacheInfo.put(request, System.currentTimeMillis());
                S3Object object = s3.getObject(new GetObjectRequest(BUCKET_NAME, request));

                InputStream inputStream = object.getObjectContent();

                pathToFile = StaticProperties.CACHE_DIR + "request" + UUID.randomUUID() + StaticProperties.TARGET_TYPE;

                FileOutputStream out = new FileOutputStream(pathToFile);

                byte[] buffer = new byte[2048];
                int bytesRead;

                while ((bytesRead = inputStream.read(buffer)) > 0)
                    out.write(buffer, 0, bytesRead);

                inputStream.close();
                out.flush();
                out.close();
            }


            cacheInfo.notifyAll();
        }

        return pathToFile;
    }

    public boolean addFileIfNotExists(String request, File file) {

        boolean returnVal = false;

        synchronized (cacheInfo) {
            if (!cacheInfo.containsKey(request)) {
                s3.putObject(new PutObjectRequest(BUCKET_NAME, request, file));

                cacheInfo.put(request, System.currentTimeMillis());

                returnVal = true;
            }

            cacheInfo.notifyAll();
        }

        return returnVal;

    }

    public static void main(String[] args) throws InterruptedException, IOException {

        Thread thisThread = new Thread(CacheManager.getInstance());

        thisThread.start();

        System.out.println("The application has added the file to the S3 storage: " +
                CacheManager.getInstance().addFileIfNotExists(
                    "123",
                    new File(
                            StaticProperties.BASE_DIR_LOCAL + "861023712" + StaticProperties.SOURCE_TYPE
                    )
                )
        );


        Thread.sleep(10000);

        System.out.println("The application has added the file to the S3 storage (this should not work): " +
                CacheManager.getInstance().addFileIfNotExists(
                        "1234",
                        new File(
                                StaticProperties.BASE_DIR_LOCAL + "861023712" + StaticProperties.SOURCE_TYPE
                        )
                )
        );

        Thread.sleep(1000);

//        System.out.println("The application has downloaded the file to " + CacheManager.getInstance().getFileIfExists("123"));
//        System.out.println("The application has downloaded the file to (this should not work)" + CacheManager.getInstance().getFileIfExists("1234"));

        System.in.read();

        CacheManager.getInstance().terminateCache();

    }

    public void terminateCache() {

        synchronized (cacheInfo) {

            ObjectListing objectListing = s3.listObjects(BUCKET_NAME);

            for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
                s3.deleteObject(new DeleteObjectRequest(BUCKET_NAME, objectSummary.getKey()));
            }

            s3.deleteBucket(BUCKET_NAME);
            cacheInfo.clear();
        }

    }


}
