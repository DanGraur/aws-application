package server;

import java.net.*;
import java.util.LinkedList;

import server.client.ClientListener;
import server.client.JobRequest;
import server.policy.job.OldestJobSelectionPolicy;
import server.policy.vm.FirstAvailableVMPolicy;
import server.schedule.Scheduler;

import java.io.*;

/**
 * The Server for IN4392-Lab
 * @author Jeff
 */
public class Server {
    
	/* thread to receive client requests */
	public static ClientListener listenerThread;
	
	/* thread to schedule client jobs to vms */
	public static Thread schedulerThread;
	
	/**
	 * The Server initializes a JobManager and ResourceManager
	 * then awaits the Client to connect and send JobRequests. 
	 * 
	 * The Server handles client requests via the ClientListener
	 * and the Server handles system simulation via the Scheduler. 
	 * The ClientListener receives JobRequests and adds them to the 
	 * JobManager. The Scheduler then continually processes each 
	 * incoming Job from the JobManager pendingQueue, and allocates 
	 * and assigns VMs as necessary, until all Jobs are complete.
	 * 
	 * The Server can be run is several modes:
	 * 	- testEcho
	 * 	- testThreading
	 * 
	 * Note: The Server must be run first.
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) {
        /* Select Server mode here */
        testThreading();
    }
	
	/**
	 * testThreading() initializes and starts the Server system. A 
	 * scheduler thread is dispatched to run the system simulation.
	 * A listener thread is dispatched to listen to the client and 
	 * handle all incoming JobRequests. 
	 * 
	 */
	public static void testThreading() {
		int portNumber = 4392;
		
		schedulerThread = new Thread(
				new Scheduler(
						new OldestJobSelectionPolicy(),
						new FirstAvailableVMPolicy()
				)
		);
		schedulerThread.start();
		
		listenerThread = new ClientListener(portNumber);
		listenerThread.start();
		
		try {
			while (true) {
				System.in.read();
				System.exit(0);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * testJob() is a basic Server that allows a client
	 * to connect, send messages as Objects, and the Server
	 * responds to the Client with the a modified message.
	 */
	@SuppressWarnings({ "resource", "unchecked" })
	public static void testJob() {
		int portNumber = 4392;

		try {
			/* Create the Server Socket */
			ServerSocket serverSocket = new ServerSocket(portNumber);

	        while (true) {
	            /* Create the Client Socket */
	            Socket clientSocket = serverSocket.accept();
	            System.out.println("Socket Extablished...");

	            /* Create input and output streams to Client */
	            ObjectOutputStream outToClient = new ObjectOutputStream(clientSocket.getOutputStream());
	            ObjectInputStream inFromClient = new ObjectInputStream(clientSocket.getInputStream());

	            /* Create JobRequest object and retrieve information */
	            LinkedList<JobRequest> inList = new LinkedList<>();
	            inList = (LinkedList<JobRequest>) inFromClient.readObject();

	            /* Send modified message back to Client */
	            String msg = inList.size() + " Jobs recieved";
	            outToClient.writeObject(msg);
	        }

	    } catch (Exception e) {
	        System.err.println("Server Error: " + e.getMessage());
	        System.err.println("Localized: " + e.getLocalizedMessage());
	        System.err.println("Stack Trace: " + e.getStackTrace());
	        System.err.println("To String: " + e.toString());
	    }
	}

	/**
	 * testEcho() is a basic Server that allows a client
	 * to connect, send messages as Strings, and the Server
	 * responds to the Client with the same String message.
	 */
    public static void testEcho() {
    		int portNumber = 4392;

        try (
            ServerSocket serverSocket =
                new ServerSocket(portNumber);
            Socket clientSocket = serverSocket.accept();
            PrintWriter out =
                new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(
                new InputStreamReader(clientSocket.getInputStream()));
        ) {
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                out.println(inputLine);
                System.out.println("client: " + inputLine);
            }

        } catch (IOException e) {
            System.out.println("Exception caught when trying to listen on port "
                + portNumber + " or listening for a connection");
            System.out.println(e.getMessage());
        }
    }
}