package server.comm;

public class SSHTransmissionException extends Exception {

    public SSHTransmissionException() {
        super("Exception encountered whilst transferring the file");
    }

    public SSHTransmissionException(String message) {
        super(message);
    }

    public SSHTransmissionException(String message, Throwable cause) {
        super(message, cause);
    }

    public SSHTransmissionException(Throwable cause) {
        super(cause);
    }

    public SSHTransmissionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
