package server.comm;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Class implementing an abstract version of the message/command being sent to the server.
 * This command can be either that of SCP send, or SCP receive, in the implementing class.
 */
public abstract class SSHMessage {

    protected final static int BUFFER_SIZE = 1024;

    /**
     * The SSH Session object
     */
    protected Session session;

    /**
     * Acknowledge object for low-level communication
     */
    protected final static byte[] ACK = {0};

    public SSHMessage(Session session) {
        this.session = session;
    }

    public Session getSession() {
        return session;
    }

    /**
     * Create an ssh communication channel
     *
     * @param command the command to be executed
     * @return the communication channel
     */
    public Channel openExecChannel(String command) throws JSchException {
        ChannelExec channel = (ChannelExec) session.openChannel("exec");
        channel.setCommand(command.getBytes());
        return channel;
    }

    /**
     * Send an ACK bit to a channel's output stream
     *
     * @param out he ouptut stream of a given channel
     */
    protected void sendAck(OutputStream out) throws IOException {
        out.write(ACK);
        out.flush();
    }

    /**
     * Await for an ACK message from the other end of the SSH connection, or simply
     *
     * @param in the input stream connected
     * @throws IOException exception thrown when input stream reads encounter issues
     * @throws SSHConnectionException exception thrown when the SSH'd server does not send an ACK, or sends an error
     */
    protected int waitForAck(InputStream in) throws IOException, SSHConnectionException {
        int statusByte = in.read();

        // This case refers to when the server did not send any response, i.e. could not read anything from the input stream
        if (statusByte == -1)
            throw new SSHConnectionException("Din not receive any response from the server when awaiting ACK");
        else if (statusByte != 0) {
            // Read the error message byte by byte
            StringBuilder errorMessage = new StringBuilder();
            int newByte;

            do {
                newByte = in.read();

                if (newByte != -1)
                    errorMessage.append(newByte);

            } while(newByte != -1);

            if (statusByte == 1)
                throw new SSHConnectionException("Received weak error when awaiting ACK: " + errorMessage);
            else if (statusByte == 2)
                throw new SSHConnectionException("Received fatal error when awaiting ACK: " + errorMessage);
            else
                throw new SSHConnectionException("Received unknown error when awaiting ACK: " + errorMessage);
        }

        return statusByte;
    }

    /**
     * Execute the command submitted in the implementing class
     */
    public abstract void execute() throws JSchException, IOException;

}
