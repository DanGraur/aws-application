package server.comm;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import java.io.IOException;
import java.io.InputStream;

public class SSHCommand extends SSHMessage {

    private String command;

    public SSHCommand(Session session, String command) {
        super(session);

        this.command = command;
    }

    @Override
    public void execute() throws JSchException, IOException {
        prepareAndSendCommand();
    }

    private void prepareAndSendCommand() throws JSchException {
        // Open a new SSH communication channel for this command
        Channel execChannel = openExecChannel(command);

        try {
            // Set the input and error stream of the channel
            execChannel.setInputStream(null);
            ((ChannelExec) execChannel).setErrStream(System.err);

            InputStream in = execChannel.getInputStream();

            execChannel.connect();

            // The actual logic
            StringBuilder response = new StringBuilder();
            byte[] buffer = new byte[BUFFER_SIZE];
            boolean flag = true;

            while(flag) {

                while (in.available() > 0) {
                    int bytesRead = in.read(buffer, 0, BUFFER_SIZE);

                    if (bytesRead == -1)
                        break;

                    response.append(new String(buffer, 0, bytesRead));
                }

                if (execChannel.isClosed()) {
                    flag = false;

                    System.out.println("Channel exit status: " + execChannel.getExitStatus());
                } else
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        // Ignore the exception
                    }
            }

            System.out.println("The command's response: " + response.toString());

        } catch (IOException e) {
            System.err.println("An IOException Exception has been encountered");

            e.printStackTrace();
        } finally {
            // Make sure to close the communication channel no matter what
            if (execChannel != null)
                execChannel.disconnect();

        }


    }


}
