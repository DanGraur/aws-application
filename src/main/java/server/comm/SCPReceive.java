package server.comm;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import java.io.*;

/**
 * Class which implements the SCP receive message
 */
public class SCPReceive extends SSHMessage {
    /**
     * The local path where the remote file will be stored
     */
    private String localPath;
    /**
     * The remote path where the remote file is located (on the server)
     */
    private String remotePath;

    public SCPReceive(Session session, String localPath, String remotePath) {
        super(session);

        this.localPath = localPath;
        this.remotePath = remotePath;
    }

    public String getLocalPath() {
        return localPath;
    }

    public String getRemotePath() {
        return remotePath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public void setRemotePath(String remotePath) {
        this.remotePath = remotePath;
    }

    @Override
    public void execute() throws JSchException {
        performSCPFromTransfer();
    }

    /**
     * Wrapper method for the SCP file transfer (receive) operation
     *
     * @throws JSchException JSch exception
     */
    private void performSCPFromTransfer() throws JSchException {
        String command = "scp -f " + remotePath;

        Channel execChannel = openExecChannel(command);

        try {
            InputStream in = execChannel.getInputStream();
            OutputStream out = execChannel.getOutputStream();

            execChannel.connect();

            receiveFile(in, out);

            /* Close the streams (just to make sure) */
        } catch (IOException e) {
            System.err.println("An IOException Exception has been encountered");

            e.printStackTrace();
        } catch (SSHConnectionException e) {
            System.err.println("An SSHConnectionException Exception has been encountered");

            e.printStackTrace();
        } finally {
            if (execChannel != null)
                execChannel.disconnect();
        }

    }

    /**
     * Performs the receive file operation
     *
     * @param in the input stream of the connection to the server
     * @param out the ouptut stream of the connection to the server
     * @throws IOException exception thrown when having issues with the streams
     * @throws SSHConnectionException ACK related exception
     */
    private void receiveFile(InputStream in, OutputStream out) throws IOException, SSHConnectionException {
        byte[] buffer = new byte[BUFFER_SIZE];

        sendAck(out);

        while(true) {
            // Read the first byte from the input stream
            int ack = in.read();

            // It must be the first character in 'C0644 ',
            // otherwise break from the reading loop
            if (ack != 'C')
                break;

            // Read the string '0644 '
            in.read(buffer, 0, 5);

            long fileSize = 0;

            // Read the file size
            while(true) {
               if (in.read(buffer, 0, 1) < 0 || buffer[0] == ' ')
                   break;

               // Reconstruct the file size digit by digit, starting from the most significant
               fileSize = fileSize * 10L + (long)(buffer[0] - '0');
            }

            String fileName = null;

            // Read the name of the file
            for (int i = 0; ; i++) {

                in.read(buffer, i, 1);

                if (buffer[i] == (byte) 0x0A) {
                    fileName = new String(buffer, 0, i);
                    break;
                }
            }

            // Send out an ACK, to request the contents of the file
            sendAck(out);

            // Create the output file, just to make sure it will be found by the output stream
            new File(localPath).createNewFile();

            // Open an output stream to it
            FileOutputStream outputStream = new FileOutputStream(localPath);

            // Read the contents of the file from the server
            while (fileSize > 0) {
                long bytesToRead = BUFFER_SIZE < fileSize ? BUFFER_SIZE : fileSize;

                // The conversion is guaranteed to work since BUFFER_SIZE fits into an int
                bytesToRead = in.read(buffer, 0, (int) bytesToRead);

                // Something went wrong, could not read anything
                if (bytesToRead < 0)
                    break;

                // Write what was read to the new file
                outputStream.write(buffer, 0, (int) bytesToRead);

                fileSize -= bytesToRead;
            }
            

            // Send a final ACK to the server
            sendAck(out);

            // Close the output stream
            outputStream.close();

        }

    }
}
