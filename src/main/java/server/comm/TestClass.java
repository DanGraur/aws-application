package server.comm;

import com.jcraft.jsch.JSchException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import server.util.CloudLogger;
import server.util.StaticProperties;

import java.io.IOException;

public class TestClass {

    public static void main(String[] args) throws JSchException, SSHConnectionException, IOException {

        PropertyConfigurator.configure("src/main/resources/log4j.properties");
        Logger logger = CloudLogger.getInstance();


        logger.info("This is an info message");

        Logger jobLogger = CloudLogger.createJobLogger(1);

        jobLogger.info("This is a job logger");



        String host = "34.193.69.150";

        SSHUserInfo userInfo = new SSHUserInfo(
                StaticProperties.VM_USERNAME,
                StaticProperties.PASSWORD,
                StaticProperties.TRUST_ENTITIES,
                StaticProperties.KEYPAIR_LOCAITON
        );
        SSHConnection connection = new SSHConnection(userInfo, host);


        try {
            connection.openSession();

            //connection.fullConversion("C:\\DAN_PC\\Facultate\\Master\\Cloud Computing\\RoialtyFreeVideos", "708213662");

            connection.fullConversionWithCompression(StaticProperties.BASE_DIR_LOCAL, "708213662");

            // Need to add a check if the remote file resides in the vm

            //connection.uploadFile("C:\\DAN_PC\\Facultate\\Master\\Cloud Computing\\RoialtyFreeVideos\\countryside.mp4", "/home/videos/asd.mp4");
            //connection.executeCommand(CommandCreator.ffmpegFromMP4toAVI("/home/videos/", "asd.mp4", "asd_converted.avi"));

            //connection.downloadFile("C:\\DAN_PC\\Facultate\\Master\\Cloud Computing\\RoialtyFreeVideos\\downloaded_new.avi", "/home/videos/asd_converted.avi");
            //connection.executeCommand(CommandCreator.removeFile("/home/videos/asd_converted.avi"));

        } catch (Exception e) {
            System.out.println("Got an exception " + e.getMessage());

            e.printStackTrace(System.out);

            throw  e;
        } finally {


            connection.closeSession();
        }



    }
}
