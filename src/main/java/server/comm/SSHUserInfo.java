package server.comm;

import com.jcraft.jsch.UserInfo;

/**
 * Class implementing UserInfo interface, required for carrying out communication via SSH
 *
 */
public class SSHUserInfo implements UserInfo {
    private String username;
    private String password;

    private boolean trustUnknonEntities;
    private String passphrase;

    private String keyFile;

    public SSHUserInfo(String username, String password, boolean trustUnknonEntities, String passphrase, String keyFile) {
        this.username = username;
        this.password = password;
        this.trustUnknonEntities = trustUnknonEntities;
        this.passphrase = passphrase;
        this.keyFile = keyFile;
    }

    public SSHUserInfo(String username, String password, boolean trustUnknonEntities, String keyFile) {
        this.username = username;
        this.password = password;
        this.trustUnknonEntities = trustUnknonEntities;
        this.keyFile = keyFile;

        // In this case, paraphrase will remain null
    }

    public SSHUserInfo(String username, boolean trustUnknonEntities, String keyFile) {
        this.username = username;
        this.trustUnknonEntities = trustUnknonEntities;
        this.keyFile = keyFile;
        this.password = "";

        // In this case, both the paraphrase will be null, and the password will be empty
    }

    public SSHUserInfo(String username, String keyFile) {
        this.username = username;
        this.keyFile = keyFile;

        this.password = "";
        this.trustUnknonEntities = true;

        // In this case, the passphrase will be null, the password empty, and the trustUnknownEntity will be true
    }

    public String getUsername() {
        return username;
    }

    public String getKeyFile() {
        return keyFile;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setYesNo(boolean yesOrNo) {
        this.trustUnknonEntities = yesOrNo;
    }

    public void setPassphrase(String passphrase) {
        this.passphrase = passphrase;
    }

    public void setKeyFile(String keyFile) {
        this.keyFile = keyFile;
    }

    @Override
    public String getPassphrase() {
        return passphrase;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public boolean promptPassword(String s) {
        return false;
    }

    @Override
    public boolean promptPassphrase(String s) {
        return false;
    }

    @Override
    public boolean promptYesNo(String s) {
        return trustUnknonEntities;
    }

    @Override
    public void showMessage(String s) {

    }

    @Override
    public String toString() {
        return "SSHUserInfo{" +
                "username='" + username + '\'' +
                ", trustUnknonEntities=" + trustUnknonEntities +
                ", keyFile='" + keyFile + '\'' +
                '}';
    }
}
