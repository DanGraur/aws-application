package server.comm.utility;

import server.util.StaticProperties;

public class CommandCreator {
    public static String DEFAULT_BASE_DIR = StaticProperties.BASE_DIR_REMOTE;

    /**
     * Creates command for converting a file from
     *
     * @param baseFolder (Linux) path to the base folder where the two files will be located
     * @param sourceFile (Linux) path to the source video
     * @param destinationFile (Linux) path to the destination video
     * @return the command which can be executed via SSH
     */
    public static String ffmpegFromMP4toAVI(String baseFolder, String sourceFile, String destinationFile) {
        // Make sure the based folder is correctly formatted
        baseFolder = baseFolder.charAt(baseFolder.length() - 1) == '/' ? baseFolder : baseFolder + '/';

        // Make sure the extensions are applied on the files
        String newSource = baseFolder + sourceFile.split("\\.")[0].concat(".mp4");
        String newDestination = baseFolder + destinationFile.split("\\.")[0].concat(".avi");

        // Return the command to be executed
        return "ffmpeg -i " + newSource + " " + newDestination + "\n";
    }

    /**
     * Creates command for removing a file
     *
     * @param files fully qualified (Linux) paths to the files being removed
     * @return the command which can be executed via SSH
     */
    public static String removeFile(String... files) {
        StringBuilder command = new StringBuilder("rm -r");

        for (String file : files)
            command.append(" ").append(file);

        command.append('\n');

        return command.toString();
    }

    /**
     * Command for tar-ing a file
     *
     * @param file the path to the file to be tarred
     * @return the command for tarring a file
     */
    public static String tarFile(String file) {
        return "tar -cf " + file.split("\\.")[0] + ".tar " + file + "\n";
    }

    /**
     * Command for tar-ing a file (should end in .tar)
     *
     * @param file the path to the file to be tarred (no hidden files)
     * @return the command for tarring a file
     */
    public static String untarFile(String file) {
        String[] pathTokens = file.split("\\.");

        return "tar -xf " + pathTokens[0] + ".tar\n";
    }

    public static String untarFileAndRemove(String file) {
        return "tar -xf " + file + " && " + removeFile(file);
    }


    public static String makeDir(String baseFolder, String dirName) {
        // Make sure the based folder is correctly formatted
        baseFolder = baseFolder.charAt(baseFolder.length() - 1) == '/' ? baseFolder : baseFolder + '/';

        return "mkdir " + baseFolder + dirName + "\n";
    }

    /**
     * Create command for full chain of actions: decompress tarball + delete tarball + use ffmpeg to convert between formats + create new tarball
     *
     * @param baseFolder the folder where the files will be stored
     * @param fileName the name of the file (no extensions)
     * @return the command for executing a full chain
     */
    public static String untar_FFmpeg_compress(String baseFolder, String fileName) {
        fileName = fileName.split("\\.")[0];
        baseFolder = baseFolder.charAt(baseFolder.length() - 1) == '/' ? baseFolder : baseFolder + '/';

        String tarName = baseFolder + fileName + ".tar";
        String sourceVideoName = baseFolder + fileName + StaticProperties.SOURCE_TYPE;
        String targetVideoName = baseFolder + fileName + StaticProperties.TARGET_TYPE;

        return String.format(
                "tar -xf %s -C %s && " +
                        "rm %s && " +
                        "ffmpeg -i %s %s &&" +
                        "tar -cf %s -C %s %s",
                tarName,
                baseFolder,
                tarName,
                sourceVideoName,
                targetVideoName,
                tarName,
                baseFolder,
                fileName + StaticProperties.TARGET_TYPE
        );
    }
}
