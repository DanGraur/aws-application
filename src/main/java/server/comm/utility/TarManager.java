package server.comm.utility;

import org.kamranzafar.jtar.TarEntry;
import org.kamranzafar.jtar.TarInputStream;
import org.kamranzafar.jtar.TarOutputStream;

import java.io.*;

public class TarManager {

    public static final String DEFAULT_EXTENSION = ".tar";
    private static final int BUFFER_SIZE = 2048;

    /**
     * Compress a singular file in tar format
     *
     * @param baseFolder the folder where the file will be located (Windows format)
     * @param fileName the name of the file which is to be compressed (extension is expected)
     */
    public static String tarSingleFile(String baseFolder, String fileName) throws IOException {
        String[] fileNameTokens = fileName.split("\\.");
        baseFolder = baseFolder.charAt(baseFolder.length() - 1) == '\\' ? baseFolder : baseFolder + '\\';

        String tarFileName = fileNameTokens[0] + DEFAULT_EXTENSION;

        TarOutputStream tarOutput = new TarOutputStream(
                new BufferedOutputStream(
                    new FileOutputStream(baseFolder + tarFileName)
                )
        );

        File compressionFile = new File(baseFolder + fileName);

        tarOutput.putNextEntry(
                new TarEntry(compressionFile, compressionFile.getName())
        );

        BufferedInputStream fileInputStream = new BufferedInputStream(
                new FileInputStream(compressionFile)
        );

        byte[] buffer = new byte[BUFFER_SIZE];
        int bytesRead;

        while((bytesRead = fileInputStream.read(buffer)) > 0)
            tarOutput.write(buffer, 0, bytesRead);

        // Cleanup
        tarOutput.flush();
        fileInputStream.close();
        tarOutput.close();

        return tarFileName;
    }

    /**
     * Decompress a singular file from tar format
     *
     * @param baseFolder the folder where the tarball will be located
     * @param fileName the name of the tarball (extension is expected)
     */
    public static void untarSingleFile(String baseFolder, String fileName) throws IOException {
        baseFolder = baseFolder.charAt(baseFolder.length() - 1) == '/' ? baseFolder : baseFolder + '/';

        TarInputStream tarInput = new TarInputStream(
                new BufferedInputStream(
                        new FileInputStream(baseFolder + fileName)
                )
        );

        // Declarations
        TarEntry entry;
        int count;
        byte[] buffer = new byte[BUFFER_SIZE];

        while((entry = tarInput.getNextEntry()) != null) {
            BufferedOutputStream outputStream = new BufferedOutputStream(
                    new FileOutputStream(
                            baseFolder + entry.getName()
                    )
            );

            while ((count = tarInput.read(buffer)) > 0)
                outputStream.write(buffer, 0, count);

            outputStream.flush();
            outputStream.close();
        }

        tarInput.close();
    }
}
