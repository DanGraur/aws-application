package server.comm;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import netscape.javascript.JSException;
import server.comm.utility.CommandCreator;
import server.comm.utility.TarManager;
import server.util.StaticProperties;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class SSHConnection {

    private static final int SSH_PORT = 22;

    private SSHUserInfo userInfo;
    private String host;
    private int port;

    private Session session;

    public SSHConnection(SSHUserInfo userInfo, String host, int port) {
        this.userInfo = userInfo;
        this.host = host;
        this.port = port;
    }

    public SSHConnection(SSHUserInfo userInfo, String host) {
        this.userInfo = userInfo;
        this.host = host;
        this.port = SSH_PORT;
    }

    public SSHUserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(SSHUserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    /**
     * Open a session with the server
     *
     * @throws JSchException when JSch exceptions are encountered
     */
    public void openSession() throws JSchException {
        JSch jsch = new JSch();

        if (userInfo.getKeyFile() != null)
            jsch.addIdentity(userInfo.getKeyFile());

        session = jsch.getSession(userInfo.getUsername(), host, port);
        session.setUserInfo(userInfo);

        //System.out.println("Default timeout: " + session.getTimeout());

        session.connect();
    }

    /**
     * Close the connection
     *
     * @throws JSchException when JSch exceptions are encountered
     */
    public void closeSession() throws JSException {
        if (session != null)
            session.disconnect();
    }

    /**
     * Transfer the file located at localPath to remotePath
     *
     * @param localPath fully qualified name of the source file to be uploaded (the file must exist)
     * @param remotePath fully qualified name of the destination file (the file needn't exist)
     * @throws SSHConnectionException thrown when ACK issues are encountered
     * @throws JSchException when JSch exceptions are encountered
     */
    public void uploadFile(String localPath, String remotePath) throws SSHConnectionException, JSchException, IOException {
        if (session == null || !session.isConnected())
            throw new SSHConnectionException("Connection not established; Please open a connection before proceeding!");

        new SCPSend(session, localPath, remotePath).execute();
    }

    /**
     * Perform SCP file transfer from remote file to local directory
     *
     * @param localPath fully qualified name of the local file to which the download will take place (the file needn't exist)
     * @param remotePath fully qualified name of the remote file to be downloaded (the file must exist)
     * @throws SSHConnectionException thrown when ACK issues are encountered
     * @throws JSchException when JSch exceptions are encountered
     */
    public void downloadFile(String localPath, String remotePath)  throws SSHConnectionException, JSchException {
        if (session == null || !session.isConnected())
            throw new SSHConnectionException("Connection not established; Please open a connection before proceeding!");

        new SCPReceive(session, localPath, remotePath).execute();
    }

    /**
     * Execute the command passed as parameter via SSH
     *
     * @param command the command to be executed
     * @throws IOException IO related issues
     * @throws JSchException when JSch exceptions are encountered
     * @throws SSHConnectionException thrown when ACK issues are encountered
     */
    public void executeCommand(String command) throws IOException, JSchException, SSHConnectionException {
        if (session == null || !session.isConnected())
            throw new SSHConnectionException("Connection not established; Please open a connection before proceeding!");

        new SSHCommand(session, command).execute();
    }

    /**
     * Performs a full conversion of a video from MP4 format to AVI format.
     *
     * @param localBasePath the path to the local folder on which files will be uploaded from, and downloaded to
     * @param fileName the name of the file to be converted (uploaded then downloaded)
     * @throws IOException throws when there are issues with the input and output streams
     * @throws JSchException when JSch exceptions are encountered
     * @throws SSHConnectionException thrown when ACK issues are encountered
     */
    public void fullConversion(String localBasePath, String fileName) throws IOException, JSchException, SSHConnectionException {
        if (session == null || !session.isConnected())
            throw new SSHConnectionException("Connection not established; Please open a connection before proceeding!");

        // Remove any file extensions
        fileName = fileName.split("\\.")[0];

        // Create the paths
        localBasePath = localBasePath.charAt(localBasePath.length() - 1) == '/' ? localBasePath : localBasePath + '/';
        String sourceFileName = fileName + ".mp4";
        String destinationFileName = fileName + ".avi";

        // Send the file MP4 to the server via SCP
        new SCPSend(session, localBasePath + sourceFileName, CommandCreator.DEFAULT_BASE_DIR + sourceFileName).execute();

        // Convert to AVI
        new SSHCommand(
                session,
                CommandCreator.ffmpegFromMP4toAVI(
                        CommandCreator.DEFAULT_BASE_DIR,
                        sourceFileName,
                        destinationFileName
                )
        ).execute();

        // Download the converted AVI file
        new SCPReceive(session, localBasePath + destinationFileName, CommandCreator.DEFAULT_BASE_DIR + destinationFileName).execute();

        // Delete both files from the server
        new SSHCommand(
                session,
                CommandCreator.removeFile(
                        CommandCreator.DEFAULT_BASE_DIR + sourceFileName, CommandCreator.DEFAULT_BASE_DIR + destinationFileName
                )
        ).execute();
    }


    /**
     * Performs a full conversion of a video from MP4 format to AVI format.
     *
     * @param localBasePath the path to the local folder on which files will be uploaded from, and downloaded to
     * @param fileName the name of the file to be converted (uploaded then downloaded)
     * @throws IOException throws when there are issues with the input and output streams
     * @throws JSchException when JSch exceptions are encountered
     * @throws SSHConnectionException thrown when ACK issues are encountered
     */
    public void fullConversionWithCompression(String localBasePath, String fileName) throws IOException, JSchException, SSHConnectionException {
        if (session == null || !session.isConnected())
            throw new SSHConnectionException("Connection not established; Please open a connection before proceeding!");

        // Remove any file extensions
        fileName = fileName.split("\\.")[0];

        // Create the paths
        localBasePath = localBasePath.charAt(localBasePath.length() - 1) == '\\' ? localBasePath : localBasePath + '\\';
        String sourceFileName = fileName + StaticProperties.SOURCE_TYPE;

        String tarFileName = TarManager.tarSingleFile(localBasePath, sourceFileName);

        // The temporary folder name of the
        String newFolderName = CommandCreator.DEFAULT_BASE_DIR + fileName + '/';

        // Create a temporary folder
        new SSHCommand(
                session,
                CommandCreator.makeDir(
                        CommandCreator.DEFAULT_BASE_DIR,
                        fileName
                )
        ).execute();


        // Send the tarball to the server
        new SCPSend(session, localBasePath + tarFileName, newFolderName + tarFileName).execute();

        // Delete the local tarball
        Files.deleteIfExists(Paths.get(localBasePath + tarFileName));

        // Compress the avi
        new SSHCommand(
                session,
                CommandCreator.untar_FFmpeg_compress(
                        newFolderName,
                        fileName
                )
        ).execute();

        // Download the converted AVI file
        new SCPReceive(session, localBasePath + tarFileName, newFolderName + tarFileName).execute();

        // Delete the folder containing the files
        new SSHCommand(
                session,
                CommandCreator.removeFile(
                        newFolderName
                )
        ).execute();

        // Decompress the tarball
        TarManager.untarSingleFile(localBasePath, tarFileName);

        // Delete the local tarball
        Files.deleteIfExists(Paths.get(localBasePath + tarFileName));



    }
}
