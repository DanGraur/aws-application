package server.comm;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import java.io.*;

/**
 * Class which implements the SCP Send messsage
 */
public class SCPSend extends SSHMessage {

    /**
     * The location of the local file to be uploaded to the server
     */
    private String localPath;

    /**
     * The remote path where the file will be stored on the server
     */
    private String remotePath;

    public SCPSend(Session session, String localPath, String remotePath) {
        super(session);
        this.localPath = localPath;
        this.remotePath = remotePath;
    }

    public String getLocalPath() {
        return localPath;
    }

    public String getRemotePath() {
        return remotePath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public void setRemotePath(String remotePath) {
        this.remotePath = remotePath;
    }

    @Override
    public void execute() throws JSchException, IOException {
        File localFile = new File(localPath);

        if (!localFile.exists())
            throw new IOException("Source file could not be found!");

        performSCPToTransfer(localFile);
    }

    /**
     * Wrapper method for the SCP file transfer (send) operation
     *
     * @param localFile the local file
     * @throws JSchException JSch exception
     */
    private void performSCPToTransfer(File localFile) throws JSchException {
        // Start building the command, by setting the remote path
        String command = "scp -t " + remotePath;

        // Open a SSH communication channel, and start sending the scp command
        Channel execChannel = openExecChannel(command);

        try {

            InputStream in = execChannel.getInputStream();
            OutputStream out = execChannel.getOutputStream();

            // Attempt to establish the connection with via SSH with the server
            execChannel.connect();

            // See if the connection was established successfully, and the start the send operation
            waitForAck(in);
            sendFile(localFile, in, out);


            /* Close the streams (just to make sure) */
            in.close();
            out.close();
        } catch (IOException e) {
            System.err.println("An IOException Exception has been encountered");

            e.printStackTrace();
        } catch (SSHConnectionException e) {
            System.err.println("An SSHConnectionException Exception has been encountered");

            e.printStackTrace();
        } finally {
            // Make sure to close the communication channel no matter what
            if (execChannel != null)
                execChannel.disconnect();

        }
    }

    /**
     * Performs the send file operation
     *
     * @param localFile the path to the local file
     * @param in the input stream of the connection to the server
     * @param out the ouptu stream of the connection to the server
     * @throws IOException exception thrown when having issues with the streams
     * @throws SSHConnectionException ACK related exception
     */
    private void sendFile(File localFile, InputStream in, OutputStream out) throws IOException, SSHConnectionException {
        long sizeOfFile = localFile.length();

        String command = "C0644 " + sizeOfFile + " " + localFile.getName() + "\n";

        // Sent the initial command
        out.write(command.getBytes());
        out.flush();

        // Wait for the server's response
        waitForAck(in);

        // Get the local file as an input stream

        // Prepare a buffer for sending the file block by block
        byte[] buffer = new byte[BUFFER_SIZE];
        //long totalLengthSent = 0;
        int len;

        try (FileInputStream fileInputStream = new FileInputStream(localFile)) {
            do {
                len = fileInputStream.read(buffer, 0, BUFFER_SIZE);

                // If there's still something to send --> send it
                if (len > 0) {
                    out.write(buffer, 0, len);
                    //totalLengthSent += len;
                }

            } while (len > 0);

            // Make sure everything is being sent
            out.flush();

            // Send an ACK out signaling that thw file writing is over
            sendAck(out);
            // Wait for an ACK from the server
            waitForAck(in);
        }


    }


}
