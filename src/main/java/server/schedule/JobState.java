package server.schedule;

/**
 * A Job can have five states.
 * @author Jeff
 *
 */
public enum JobState {
	PENDING,
	RUNNING,
	FINISHED,
	FAILED,
	STOPPED;

	public static String stateToString(JobState state) {
		switch (state) {
			case FINISHED: return "FINISHED";
			case PENDING: return "PENDING";
			case RUNNING: return "RUNNING";
			case FAILED: return "FAILED";
			default: return "STOPPED";
		}
	}

}
