package server.schedule;

import org.apache.log4j.Logger;
import server.policy.job.JobSelectionPolicy;
import server.util.CloudLogger;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * The JobManager is a singleton responsible for maintaining the 
 * three Job queues as well as managing how Jobs are run
 * through an ExecutorService, or thread pool.
 * @author Jeff
 */
public class JobManager implements Runnable {

	Logger logger = CloudLogger.getInstance();

	/* singleton of JobManager */
	private static JobManager _instance;
	
	/* thread pool for running jobs */
	private ExecutorService jobThreadPool;
	
	/* three job queues */
	private final JobQueue pendingJobQueue;
	private final JobQueue runningJobQueue;
	private final JobQueue finishedJobQueue;

	private boolean interrupt;

	/**
	 * The default sleep time between re-provisioning events
	 */
	private static long WAIT_TIME = 10 * 1000;

	/**
	 *  JobManager constructor initializes thread pool and three queues
	 */
	private JobManager() {
		
		this.jobThreadPool = Executors.newCachedThreadPool();
		
		this.pendingJobQueue = new JobQueue();
		this.runningJobQueue = new JobQueue();
		this.finishedJobQueue = new JobQueue();

		interrupt = false;
	}
	/**
	 * Handle on JobManager singleton instance
	 * @return JobManager
	 */
	public static JobManager instance() {
		if (_instance == null)
			_instance = new JobManager();

		return _instance;
	}
	
	/**
	 * Runs a job on one of the threads and adds Job to the running queue. It is assumed that the job
	 * has been (already) removed from the pending job queue.
	 *
	 * @param job
	 */
	public void runJob(Job job) {
		this.jobThreadPool.execute(job);
		this.addRunningJob(job);
	}


	public void waitOnPendingJobQueue() throws InterruptedException {
		synchronized (pendingJobQueue) {
			pendingJobQueue.wait();
		}
	}
	
	public int getPendingJobQueueSize() {
		return this.pendingJobQueue.size();
	}

	public void addPendingJob(Job job) {
		pendingJobQueue.addJob(job);
	}

	public void removePendingJob(Job job) {
		pendingJobQueue.removeJob(job);
	}

	public int getRunningJobQueueSize() {
		return this.runningJobQueue.size();
	}
	
	public void addRunningJob(Job job) {
		this.runningJobQueue.addJob(job);
	}
	
	public void removeRunningJob(Job job) {
		this.runningJobQueue.removeJob(job);
	}
	
	public int getFinishedJobQueueSize() {
		return this.finishedJobQueue.size();
	}
	
	public void addFinishedJob(Job job) {
		this.finishedJobQueue.addJob(job);
	}
	
	public void removeFinishedJob(Job job) {
		this.finishedJobQueue.removeJob(job);
	}

	public void setInterrupt(boolean interrupt) {
		this.interrupt = interrupt;
	}

	/**
	 * Updates the status of each Job and prints out a summary
	 */
	public void run() {

		while (!interrupt) {
			long time = System.currentTimeMillis();
			int pendingJobs = this.getPendingJobQueueSize();
			int runningJobs = this.getRunningJobQueueSize();
			int finishedJobs = this.getFinishedJobQueueSize();

		/* Record information on jobs in each of the 3 queues */

		/*
		 *	The following instruction is a bit redundant. It refers to waiting time updates, which should be relevant to
		 * 	a job selection policy which selects jobs based on waiting times. However, this can be done directly in the policy
		 * 	itself, since it fits there more organically. In any case, I've added it here.
		 */
			pendingJobQueue.updateWaitingTime();

			String msg = "========================================";
			logger.info(msg);
			msg = String.format(
					"Time: %d.", time);
			logger.info(msg);
			msg = String.format(
					"Jobs in pending queue: %d.", pendingJobs);
			logger.info(msg);
			msg = String.format(
					"Jobs in running queue: %d.", runningJobs);
			logger.info(msg);
			msg = String.format(
					"Jobs in finished queue: %d.", finishedJobs);
			logger.info(msg);
			msg = "========================================";
			logger.info(msg);

			try {
				Thread.sleep(WAIT_TIME);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}


		logger.info("The Job Manager thread has been interrupted. Will begin cleanup.");

		cleanup();

		logger.info("The Job Manager thread has finished cleanup. Will finish execution now...");
		logger.info("The Job Manager thread has finished cleanup. Will finish execution now...");
		logger.info("The Job Manager thread has finished cleanup. Will finish execution now...");
		logger.info("The Job Manager thread has finished cleanup. Will finish execution now...");
		logger.info("The Job Manager thread has finished cleanup. Will finish execution now...");
		logger.info("The Job Manager thread has finished cleanup. Will finish execution now...");
		logger.info("The Job Manager thread has finished cleanup. Will finish execution now...");
		logger.info("The Job Manager thread has finished cleanup. Will finish execution now...");
		logger.info("The Job Manager thread has finished cleanup. Will finish execution now...");
		logger.info("The Job Manager thread has finished cleanup. Will finish execution now...");
		logger.info("The Job Manager thread has finished cleanup. Will finish execution now...");
	}

	/**
	 * Thread cleanup
	 */
	private void cleanup() {
		/* List of jobs that will hold jobs forcefully stopped */
		List<Runnable> stoppedJobs = null;

		try {
			if (jobThreadPool.awaitTermination(60, TimeUnit.SECONDS))
				logger.info("All jobs terminated successfully");
			else {
				stoppedJobs = jobThreadPool.shutdownNow();

				logger.info("Not all jobs finished successfully. They will be terminated now");
			}

		} catch (InterruptedException e) {
			/* If interrupted kill all jobs now */
			stoppedJobs = jobThreadPool.shutdownNow();

			logger.error("The JobManager was interrupted whilst waiting for the jobs to finish. All jobs will be terminated now, regardless");
		}

		if (stoppedJobs != null && !stoppedJobs.isEmpty())
			for (Runnable runnable : stoppedJobs)
				((Job) runnable).setState(JobState.STOPPED);

		logger.info("Will record final states of all the jobs");

		pendingJobQueue.recordFinalState();
		runningJobQueue.recordFinalState();
		finishedJobQueue.recordFinalState();


	}

	/**
	 * Selects a pending job for vm execution
	 *
	 * @param jobSelectionPolicy the job selection policy
	 * @return the selected job
	 */
    public Job getPendingJob(JobSelectionPolicy jobSelectionPolicy) {
		return jobSelectionPolicy.selectJob(pendingJobQueue);
    }

}

