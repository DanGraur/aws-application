package server.schedule;

import org.apache.log4j.Logger;
import server.cache.CacheManager;
import server.policy.job.JobSelectionPolicy;
import server.policy.vm.VMSelectionPolicy;
import server.resource.ResourceManager;
import server.resource.VMInstance;
import server.util.CloudLogger;
import sun.misc.Cache;

import java.io.IOException;
import java.util.Random;

/**
 * The Scheduler is a runnable class that runs the simulation.
 * 
 * It controls retrieving Jobs from the JobManager, retrieving VMInstances
 * from the ResourceManager, and pairing them so Jobs can run 
 * successfully and complete.
 * @author Jeff
 */
public class Scheduler implements Runnable {

	private JobSelectionPolicy jobSelectionPolicy;
	private VMSelectionPolicy vmSelectionPolicy;

	private ResourceManager resourceManager;
	private JobManager jobManager;

	private CacheManager cacheManager;

	private Logger logger = CloudLogger.getInstance();

	private boolean interrupt;

	/**
	 * Schedule'r constructor
	 *
	 * @param jobSelectionPolicy the policy for selecting jobs
	 * @param vmSelectionPolicy the policy for selecting a VM
	 */
	public Scheduler(JobSelectionPolicy jobSelectionPolicy, VMSelectionPolicy vmSelectionPolicy) {
		this.jobSelectionPolicy = jobSelectionPolicy;
		this.vmSelectionPolicy = vmSelectionPolicy;

		resourceManager = ResourceManager.instance();
		jobManager = JobManager.instance();

		cacheManager = CacheManager.getInstance();

		interrupt = false;
	}

	public void setInterrupt(boolean interrupt) {
		this.interrupt = interrupt;
	}

	/**
	 * The simulation of pairing jobs to VMs and for job completion.
	 */
	@Override
	public void run() {
		logger.info("The Scheduler thread has started.");

		Random random = new Random();

		try {
			while(!interrupt) {
				/* Update resourceManager */
				/* The resource manager has a run method, which means it will likely not need any sort of invocation to update its content */
				//resourceManager.update();

				/* Select Job from pendingQueue*/
				Job job = jobManager.getPendingJob(jobSelectionPolicy);

				/* Wait until there is an available job to be executed */
				while (job == null) {
					logger.info("Could not find job for execution. Will wait until new job appears.");

					jobManager.waitOnPendingJobQueue();

					logger.info("Thread woke up. Looking for a job.");

					job = jobManager.getPendingJob(jobSelectionPolicy);
				}

				if (random.nextDouble() < 0.35) {
					try {
						cacheManager.getFileIfExists("1234");
					} catch (IOException e) {
						e.printStackTrace();
					}

					job.setState(JobState.FINISHED);
					jobManager.addFinishedJob(job);

					continue;
				}

				logger.info("Thread found job: + " + job.toString());

				/* Select available VM from resources*/
				VMInstance vm = resourceManager.getAvailableVM(vmSelectionPolicy, job);

				/* Note: the task of creating new VMs will be left to the Resource Manager. The scheduler does not care about that */
				while (vm == null) {
					logger.info("Could not find VM for execution. Will wait until a VM will be idle.");

					resourceManager.waitOnVMs();

					logger.info("Thread woke up. Looking for a VM.");

					vm = resourceManager.getAvailableVM(vmSelectionPolicy, job);
				}

				logger.info(String.format("I've selected job %s for execution", job.toString()));
				logger.info(String.format("I've selected VM %s for execution", vm.toString()));
				logger.info("Will begin to run the job on the selected instance");

				/* Set the VM to a busy state */
				vm.setBusy(true);
				logger.info(String.format("VM #%s has been set to busy", vm.getId()));

				/* Associate the VM to the current task */
				job.associateVM(vm);
				logger.info(String.format("VM #%s has been associated to Job #%d", vm.getId(), job.getId()));

				/* Start executing the job on the selected VM */
				jobManager.runJob(job);
			}
		} catch (InterruptedException e) {
			logger.error("Interrupted whilst waiting for either a job or VM", e);
		}

		logger.info("The Scheduler thread has been interrupted. Will begin cleanup.");

		cleanup();

		logger.info("The Scheduler thread has finished cleanup. Will finish execution now...");

	}

	/**
	 * Thread cleanup
	 */
	private void cleanup() {
		/*
		* The Scheduler thread doesn't have much to clean up. It will just set
		* its fields to null (this doesn't really do much).
		*/
		vmSelectionPolicy = null;
		jobSelectionPolicy = null;
		jobManager = null;
		resourceManager = null;
	}
}
