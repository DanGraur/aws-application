package server.schedule;

import org.apache.log4j.Logger;
import server.client.JobRequest;
import server.resource.VMInstance;
import server.stats.Analyzer;
import server.util.CloudLogger;
import server.util.StaticProperties;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A Job is a runnable object that contains information necessary to
 * convert a video file.
 * @author Jeff
 */
public class Job implements Runnable{

	private Logger jobLogger;

	/* job IDs start at 0, and are unique */
	private static int jobId = 0;
	
	/* job information */
	private int id;
	private JobState state;
	private VMInstance vmInstance;
	private long startTime;
	private long waitTime;
	private int failures;
	private long executionTime;

	/* job parameters */
	private JobRequest request;
	
	/* job constructor from JobRequest */
	public Job(int id, JobRequest request) {
		this.id = id;
		this.state = JobState.PENDING;
		this.vmInstance = null;
		this.startTime = System.currentTimeMillis();
		this.waitTime = 0;
		this.failures = 0;
		this.executionTime = 0L;
		
		this.request = request;

		jobLogger = CloudLogger.createJobLogger(id);
		jobLogger.info(String.format("Job #%d has been created", id));
	}
	
	/**
	 * @return a unique jobId
	 */
	public synchronized static int generateJobID() {
		return jobId++;
	}
	
	public void setWaitTime() {
		this.waitTime = System.currentTimeMillis() - this.startTime;
	}
	
	public long getWaitTime() {
		return this.waitTime;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return this.id;
	}
	
	public void setState(JobState state) {
		this.state = state;
	}
	public JobState getState() {
		return this.state;
	}
	
	public int getFailures() {
		return this.failures;
	}

	public void associateVM(VMInstance vmInstance) {
		this.vmInstance = vmInstance;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		
		Job job = (Job)obj;
		if (this.id != job.id) return false;
		
		return true;
	}
	
	/**
	 * Method to run a Job.
	 */
	@Override
	public void run() {
		
		try {
			this.state = JobState.RUNNING;

			/* Start running the Job */
			jobLogger.info("Starting the job...");

			/* Adjust the waiting time of this job */
			setWaitTime();

			long sTime = System.currentTimeMillis();

			vmInstance.getVmConnection().fullConversion(
					StaticProperties.BASE_DIR_LOCAL,
					request.getRequest()
			);

			executionTime = System.currentTimeMillis() - sTime;
			/* Add to the active time of the VM instance */
			vmInstance.addActiveTime(executionTime);

			/* Retrieve data for analysis */
			Analyzer.getInstance().addWaitingTime(waitTime);
			Analyzer.getInstance().addExecutionTime(executionTime);
			Analyzer.getInstance().addMakepan(waitTime + executionTime);

			/* At this point, the job will have been completely finished */
			jobLogger.info("Job finished successfully.");

			/* Also this is where caching might occur, for instance */

			this.state = JobState.FINISHED;
		} catch (Exception e) {
			jobLogger.error(String.format("JOB exception during execution: %s.", e.getMessage()), e);

			/* No need to reset the waiting time */
			//this.waitTime = 0;

			/* Set the job's current state */
			this.state = JobState.FAILED;

			/* Increment this job's number of failures */
			++this.failures;
		} finally {
			/* Disassociate the VM from this job */
			jobLogger.info(String.format("Disassociating VM instance #%s from this job.", vmInstance.getId()));
			vmInstance.setBusy(false);
			vmInstance.writeToLogger(String.format("I have been disassociated from job #%d", jobId));
			vmInstance = null;

			/* Remove the current instance from the pending job queue */
			jobLogger.info("Removing the job from the running queue");
			JobManager.instance().removeRunningJob(this);

			if (state.equals(JobState.FINISHED)) {
				/* Ad the current job to the finished queue */
				JobManager.instance().addFinishedJob(this);
				jobLogger.info("I have been added to the finished job queue.");
			}
			else {
				/* If we reach this point, it means the current job has failed */
				JobManager.instance().addPendingJob(this);
				jobLogger.info("I have been added to the pending job queue.");

				/* Set the job's current state as pending */
				state = JobState.PENDING;
			}
		}
	}

	@Override
	public String toString() {
		return "Job{" +
				"id=" + id +
				", state=" + state +
				", vmInstance='" + vmInstance + '\'' +
				", startTime=" + startTime +
				", waitTime=" + waitTime +
				", failures=" + failures +
				", request=" + request +
				'}';
	}

	public void recordFinalState() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MMMMM.yyyy hh:mm:ss:SSS");

		/* Disassociate the VMInstance */
		vmInstance = null;

		/* Write the final status of the Job*/
		jobLogger.info(
				String.format(
						"Application shutting down. Final state of Job %d is:\n" +
								" - State: %s\n" +
								" - File Request: %s\n" +
								" - Start Time: %s\n" +
								" - Wait Time: %d ms\n" +
								" - Failures: %d\n" +
								" - Actual Execution Time: %d ms\n" ,
						id,
						JobState.stateToString(state),
						request.getRequest(),
						dateFormat.format(new Date(startTime)),
						waitTime,
						failures,
						executionTime
				)

		);

	}
}
