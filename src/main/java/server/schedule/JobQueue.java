package server.schedule;

import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * A JobQueue is a thread safe class for storing a list Jobs
 * that supports Queue methods.
 * @author Jeff
 */
public class JobQueue {

	/* A LinkedList of Jobs */
	private final LinkedList<Job> queue;
	
	public JobQueue() {
		this.queue = new LinkedList<Job>();
	}
	
	public LinkedList<Job> getInstance() {
		return queue;
	}
	
	public boolean isEmpty() {
		boolean empty;

		synchronized (queue) {
			empty = queue.isEmpty();
		}

		return empty;
	}

	public int size() {
		synchronized (queue) {
			return queue.size();
		}
	}
	
	public void addJob(Job job) {
		synchronized (queue) {
			queue.add(job);
		}
		synchronized (this) {
			/* notify all the waiting threads */
			this.notifyAll();
		}
	}
	
	public void removeJob(Job job) {
		synchronized (queue) {
			queue.remove(job);
		}
		synchronized (this) {
			/* notify all the waiting threads */
			this.notifyAll();
		}
	}
	
	public Job popJob() {
		Job job = null;

		synchronized (queue) {
			if (!queue.isEmpty())
				job = queue.pop();
		}

		return job;
	}
	
	public void sort(Comparator<Job> comparator) {
		synchronized (queue) {
			queue.sort(comparator);
		}
	}

	/**
	 * Updates the waiting time of the jobs in the queue
	 */
    public void updateWaitingTime() {

		synchronized (queue) {
			Iterator<Job> iterator = queue.iterator();

			while(iterator.hasNext()) {
				Job job = iterator.next();

				synchronized (job) {
					job.setWaitTime();
				}
			}
		}

    }

	/**
	 * Record the final state, before shutdown, of each job in the queue
	 */
	public void recordFinalState() {

    	synchronized (queue) {
			for (Job job : queue) {
				synchronized (job) {
					job.recordFinalState();
				}
			}
		}
    }
}
