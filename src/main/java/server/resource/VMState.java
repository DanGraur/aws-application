package server.resource;

/**
 * The states of an EC2 instance
 */
public enum VMState {
	UNKNOWN			(-1),
	PENDING 		(0),
	RUNNING 		(16),
	SHUTTING_DOWN	(32),
	TERMINATED 		(48),
	STOPPING		(64),
	STOPPED			(80);

	private final Integer state;

	VMState(int state) {
		this.state = state;
	}

	public Integer getState() {
		return state;
	}

	public static VMState integerToState(Integer val) {
		switch (val) {
			case 0: return PENDING;
			case 16: return RUNNING;
			case 32: return SHUTTING_DOWN;
			case 48: return TERMINATED;
			case 64: return STOPPING;
			case 80: return STOPPED;
			default: return UNKNOWN;
		}
	}

	public static Integer stateToInteger(VMState state) {
		switch (state) {
			case PENDING: return 0;
			case RUNNING: return 16;
			case SHUTTING_DOWN: return 32;
			case TERMINATED: return 48;
			case STOPPING: return 64;
			case STOPPED: return 80;
			default: return -1;
		}

	}
}
