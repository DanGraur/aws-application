package server.resource;

import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.jcraft.jsch.JSchException;
import org.apache.log4j.Logger;
import server.comm.SSHConnection;
import server.comm.SSHUserInfo;
import server.util.CloudLogger;
import server.util.EC2ClientAccessor;
import server.util.StaticProperties;

/**
 * The VirtualMachine is a runnable class that creates a VMInstance 
 * and adds it to the VM list
 *
 */
public class VMHandle implements Runnable{

	private Logger logger = CloudLogger.getInstance();

	/* The maximal waiting time for booting up will be 2 min */
	private final long BOOT_TIMEOUT = 2 * 60 * 1000;
	private final long WAIT_TIME = 5000;
	private final int SESSION_RESTARTS = 5;

	/**
	 * Identifies the current state of the VMInstance
	 */
	private VMHandleState handleState = VMHandleState.PENDING;

	public VMHandleState getHandleState() {
		return handleState;
	}

	/**
	 * Run method which attempts to create a VM instance
	 */
	@Override
	public void run() {

		logger.info("Starting a new EC2 VM...");

		AmazonEC2Client ec2Client = EC2ClientAccessor.getInstance();

		RunInstancesRequest ec2Request = new RunInstancesRequest();

		logger.info(String.format(
				'\n' +
				" - Image: %s\n" +
				" - Instance Type: %s\n" +
				" - Access Group: %s\n" +
				" - Key Name: %s\n" +
				" - Min Count: %d\n" +
				" - Max Count: %d\n",
				StaticProperties.IMG_TYPE,
				StaticProperties.VM_TYPE,
				StaticProperties.ACCESS_GROUP,
				StaticProperties.KEYPAIR_NAME,
				StaticProperties.MIN_INSTANCES,
				StaticProperties.MAX_INSTANCES
		));

		ec2Request
				.withImageId(StaticProperties.IMG_TYPE)
				.withInstanceType(StaticProperties.VM_TYPE)
				.withMinCount(StaticProperties.MIN_INSTANCES)
				.withMaxCount(StaticProperties.MAX_INSTANCES)
				.withKeyName(StaticProperties.KEYPAIR_NAME)
				.withSecurityGroups(StaticProperties.ACCESS_GROUP);

		logger.info("Attempting to create an EC2 instance");
		RunInstancesResult ec2Result = ec2Client.runInstances(ec2Request);

		logger.info("Created an EC2 instance. Waiting for it to start running");
		/* Since only one instance is created, we can retrieve only the first one in the reservation */
		Instance instance = ec2Result.getReservation().getInstances().get(0);

		logger.info(String.format("Created EC2 instance, having id: %s", instance.getInstanceId()));
		/* Create the VMInstance */
		VMInstance vmInstance = new VMInstance(instance.getInstanceId(), instance);

		vmInstance.writeToLogger("Instance started, waiting to enter a running state");
		logger.info(String.format("Waiting for EC2 instance %s to enter a running state", instance.getInstanceId()));

		try {
			long startTime = System.currentTimeMillis();

			while (!vmInstance.getTrueState().equals(VMState.RUNNING)) {

				/* The system could not boot up in time */
				if (System.currentTimeMillis() - startTime > BOOT_TIMEOUT) {
					logger.error(String.format("VM instance #%s could not boot up in time. Shutting it down", vmInstance.getId()));
					vmInstance.writeToLogger("The VM instance could not boot up. I'm getting shut down");

					throw new VMStartupException(String.format("VM instance #%s could not boot up in time. Shutting it down.", vmInstance.getId()));
				}

				try {
					Thread.sleep(WAIT_TIME);
				} catch (InterruptedException e) {
					logger.error("Was interrupted whilst waiting", e);
				}
			}

			vmInstance.writeToLogger("Instance has started. Will add an IP to it.");
			logger.info(String.format("EC2 instance %s has entered a running state", instance.getInstanceId()));
			logger.info("Creating an ElasticIP and binding it to the EC2 instance");
			// Create a request and get a result for the ip allocation
//			AllocateAddressRequest addressRequest = new AllocateAddressRequest().withDomain(DomainType.Vpc);
//			AllocateAddressResult addressResult = ec2Client.allocateAddress(addressRequest);
//
//			logger.info("Have created an ElasticIP");
//			// Get the id of the allocation
//			String allocationId = addressResult.getAllocationId();
//
//			// Associate the ip to the EC2 instance
//			AssociateAddressRequest associateAddressRequest = new AssociateAddressRequest().withAllocationId(allocationId).withInstanceId(instance.getInstanceId());
//			AssociateAddressResult associateAddressResult = ec2Client.associateAddress(associateAddressRequest);
//
//			vmInstance.setAllocationId(associateAddressRequest.getAllocationId());
//			vmInstance.setIpAddress(EC2ClientAccessor.getInstanceIp(vmInstance.getId()));
			vmInstance.setIpAddressInternal();
			logger.info("Have paired the ElasticIP to the instance");
			vmInstance.writeToLogger("Have paired the ElasticIP to this instance. Creating an SSH connecton object for it.");

			logger.info("Creating the UserInfo for VM instance " + vmInstance.getId());
			SSHUserInfo userInfo = new SSHUserInfo(
					StaticProperties.VM_USERNAME,
					StaticProperties.TRUST_ENTITIES,
					StaticProperties.KEYPAIR_LOCAITON
			);

			boolean connected = false;

			for (int i = 1; i <= SESSION_RESTARTS && !connected; ++i) {
				try {
					vmInstance.setIpAddressInternal();

					logger.info(String.format("Attaching %s to VM %s", userInfo.toString(), vmInstance.getId()));
					vmInstance.setAndOpenVmConnection(
							new SSHConnection(
									userInfo,
									vmInstance.getIpAddress()
							)
					);

					connected = true;
				} catch (JSchException e) {
					if (i != SESSION_RESTARTS)
						logger.info("Connection timeout. Trying again...");
					else {
						logger.info("Could not connect. Giving up, and proceeding with shutdown...");
						throw e;
					}
				}
			}

			vmInstance.writeToLogger("SSH object attached successfully. Instance successfully created.");

			/* If the thread has been interrupted, throw an exception here, and kill it */
			if (Thread.interrupted())
				throw new VMStartupException("Startup interrupted. Need to shut down...");

			vmInstance.writeToLogger(String.format("Adding VM instance #%s to the Resource Manager", vmInstance.getId()));
			ResourceManager.instance().addVM(vmInstance);
			vmInstance.markStartTime();
			vmInstance.writeToLogger(String.format("VM instance #%s has been successfully added to the Resource Manager", vmInstance.getId()));

			logger.info("Setting the state of this VMHandle to SUCCESS");
			handleState = VMHandleState.SUCCESS;
		} catch (VMStartupException e) {
			logger.error(String.format("In catch block. Could not start VM. Attempting to terminate VM #%s...", vmInstance.getId()), e);

			vmInstance.terminateThisInstance();

			logger.info(String.format("VM #%s has been terminated successfully.", vmInstance.getId()));

			logger.info("Setting the state of this VMHandle to FAILED");
			handleState = VMHandleState.FAIL;
		} catch (JSchException e) {
			logger.error(String.format("In catch block. Could not establish SSH connection. Attempting to terminate VM #%s...", vmInstance.getId()), e);

			vmInstance.terminateThisInstance();

			logger.info(String.format("VM #%s has been terminated successfully.", vmInstance.getId()));

			logger.info("Setting the state of this VMHandle to FAILED");
			handleState = VMHandleState.FAIL;
		}


		logger.info(String.format("VM #%S launched successfully", vmInstance.getId()));
	}

}
