package server.resource;

import com.amazonaws.services.ec2.model.*;
import com.jcraft.jsch.JSchException;
import org.apache.log4j.Logger;
import server.comm.SSHConnection;
import server.stats.Analyzer;
import server.stats.VMInstanceExecInfo;
import server.util.CloudLogger;
import server.util.EC2ClientAccessor;

/**
 * Wrapper class for the EC2 instance
 */
public class VMInstance {

	/**
	 * The instance's internal logger
	 */
	private Logger internalLogger;

	/**
	 * The VM's ID
	 */
	private String id;

	/**
	 * The VM's state
	 */
	private volatile VMState state;

	/**
	 * The public ip of the VM
	 */
	private volatile String ipAddress;

	/**
	 * The allocation id will be used to release the ElasticIP from the VM instance
	 */
	private String allocationId;

	/**
	 * This object represents the communcation medium with the VM
	 */
	private SSHConnection vmConnection;

	/**
	 * The EC2 instance object
	 */
	private Instance instance;

	/**
	 * Flah which indicates if this instance is currently running a job
	 */
	private boolean isBusy;

	/**
	 * Represents the total running time
	 */
	private long startTime = -1L;

	/**
	 *
	 */
	private long activeTime;

	public VMInstance(String id, String ipAddress, Instance instance) {
		this.id = id;
		this.ipAddress = ipAddress;
		this.instance = instance;
		this.isBusy = false;
		this.activeTime = 0;

		internalLogger = CloudLogger.createInstanceLogger(this.id);
	}

	public VMInstance(String id, Instance instance) {
		this.id = id;
		this.instance = instance;
		this.activeTime = 0;

		/* Default value for the instance state when it's just been created */
		this.state = VMState.UNKNOWN;
		this.isBusy = false;

		internalLogger = CloudLogger.createInstanceLogger(this.id);
	}

	/**
	 * Sets (hardcodes) the current startup time of the system.
	 */
	public void markStartTime() {
		startTime = System.currentTimeMillis();
	}

	@Override
	public String toString() {
		return "VMInstance{" +
				"id=" + id +
				", state=" + state +
				", ipAddress='" + ipAddress + '\'' +
				'}';
	}

	public String getId() {
		return id;
	}

	public VMState getState() {
		return state;
	}

	public void setState(VMState state) {
		this.state = state;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public boolean isBusy() {
		return isBusy;
	}

	public void setBusy(boolean busy) {
		isBusy = busy;
	}

	public String getAllocationId() {
		return allocationId;
	}

	public void setAllocationId(String allocationId) {
		this.allocationId = allocationId;
	}

	public void updateStats() {
		/* Get the total runtime of the system */
		startTime = System.currentTimeMillis() - startTime;

		/* Get some data for analysis */
		Analyzer.getInstance().addVmInfo(
				new VMInstanceExecInfo(
						id,
						startTime,
						activeTime
				)
		);

	}

	public void terminateThisInstance() {

		/* Closing the instance's SSH connection */
		if (vmConnection != null) {
			internalLogger.info("Closing the SSH connection");
			vmConnection.closeSession();
			internalLogger.info("SSH Connection closed");
		}

		/* Need to also disassociate and release the IP */
		if (allocationId != null) {
			internalLogger.info("Releasing the ElasticIP");
			ReleaseAddressRequest request = new ReleaseAddressRequest().withAllocationId(allocationId);
			EC2ClientAccessor.getInstance().releaseAddress(request);
			internalLogger.info("ElasticIP release succesfully");
		}

		/* Terminate the VM instance */
		if (instance != null) {
			internalLogger.info("Terminating the EC2 instance");
			TerminateInstancesRequest terminateInstancesRequest = new TerminateInstancesRequest().withInstanceIds(instance.getInstanceId());
			TerminateInstancesResult terminateInstancesResult = EC2ClientAccessor.getInstance().terminateInstances(terminateInstancesRequest);

			InstanceStateChange terminateInstance = terminateInstancesResult.getTerminatingInstances().get(0);
			CloudLogger.getInstance().info(
					String.format(
							"Terminating Instance\n" +
									" - Instance: %s\n" +
									" - Current State: %s\n",
							terminateInstance.getInstanceId(),
							terminateInstance.getCurrentState()
					)
			);

			internalLogger.info("EC2 instance terminated");
			internalLogger.info(
					String.format(
							"Terminating Instance\n" +
									" - Instance: %s\n" +
									" - Current State: %s\n",
							terminateInstance.getInstanceId(),
							terminateInstance.getCurrentState()
					)
			);

			/* Save runtime information about the of the VM*/
			if (startTime != -1L)
				internalLogger.info(
						String.format(
								"My total runtime was: %d\n" +
										"My active runtime was: %d\n" +
										"Fractino active: %f",
								startTime,
								activeTime,
								activeTime / (double) startTime
						)
				);

			/* Remove this from the resource pool */
			internalLogger.info("Removing this instance from the resource pool");
			ResourceManager.instance().removeVM(this);
		}

		/* Set the state of the instance to TERMINATED */
		state = VMState.TERMINATED;
	}

	public SSHConnection getVmConnection() {
		return vmConnection;
	}

	public void setVmConnection(SSHConnection vmConnection) {
		this.vmConnection = vmConnection;
	}

	public void writeToLogger(String s) {
		internalLogger.info(s);
	}

	public void setAndOpenVmConnection(SSHConnection andOpenVmConnection) throws JSchException {
		this.vmConnection = andOpenVmConnection;

		vmConnection.openSession();
	}

	/**
	 * Returns the true state of the EC2 instance by making a request to the AWS servers
	 *
	 * @return the true state of the instance
	 */
	public VMState getTrueState() {
		DescribeInstancesRequest describeInstancesRequest = new DescribeInstancesRequest().withInstanceIds(id);
		DescribeInstancesResult describeInstancesResult =
				EC2ClientAccessor
						.getInstance()
						.describeInstances(describeInstancesRequest);


		InstanceState AWSState = describeInstancesResult.getReservations().get(0).getInstances().get(0).getState();
		state = VMState.integerToState(AWSState.getCode());

		return state;
	}

	public void addActiveTime(long executionTime) {
		activeTime += executionTime;
	}

	public void setIpAddressInternal() {
		DescribeInstancesRequest describeInstancesRequest = new DescribeInstancesRequest().withInstanceIds(id);
		DescribeInstancesResult describeInstancesResult =
				EC2ClientAccessor
						.getInstance()
						.describeInstances(describeInstancesRequest);


		ipAddress = describeInstancesResult.getReservations().get(0).getInstances().get(0).getPublicIpAddress();
	}
}
