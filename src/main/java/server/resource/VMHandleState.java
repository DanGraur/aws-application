package server.resource;

/**
 * @author Dan Graur 10/31/2017
 */
public enum VMHandleState {

    /**
     * This is the default state. It suggests that a VM instance is currently being created,
     * however, the outcome of this creation is not yet known.
     */
    PENDING,

    /**
     * This state means that the VM instance creation has successfully terminated.
     */
    SUCCESS,

    /**
     * This state means that the VM instance creation has failed.
     */
    FAIL,


}
