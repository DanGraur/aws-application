package server.resource;

import org.apache.log4j.Logger;
import server.policy.provisioning.leasing.LeasingProvisioningPolicy;
import server.policy.provisioning.leasing.SimpleVMLeasingPolicy;
import server.policy.provisioning.releasing.ReleasingProvisioningPolicy;
import server.policy.provisioning.releasing.SimpleVMReleasingPolicy;
import server.policy.vm.VMSelectionPolicy;
import server.schedule.Job;
import server.schedule.JobManager;
import server.util.CloudLogger;
import server.util.StaticProperties;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * The ResourceManager is a singleton responsible for maintaining the 
 * list of VMInstances as well as managing how VirtualMachines are run
 * through an ExecutorService, or thread pool.
 *
 */
public class ResourceManager implements Runnable {

	/**
	 * The global logger object
	 */
	private Logger logger = CloudLogger.getInstance();

	/**
	 * Singleton instance
	 */
	private static ResourceManager _instance;

	/**
	 *  Will host a list of active VMs
	 */
	private final LinkedList<VMInstance> vmList;

	/**
	 * Will host threads initializing VMs
	 */
	private ExecutorService vmThreadPool;

	/**
	 * VM provisioning policy
	 */
	private LeasingProvisioningPolicy leasingPolicy;
	private ReleasingProvisioningPolicy releasingPolicy;

	/**
	 * List of Futures representing previous VMHandle requests
	 */
	private List<Future<?>> vmInstancesRequests;

	/**
	 * The default sleep time between re-provisioning events
	 */
	private static long WAIT_TIME = 10 * 1000;


	private boolean interrupt;

	/**
	 * handle on singleton instance of ResourceManager
	 * @return ResourceManager
	 */
	public static ResourceManager instance() {
		if (_instance == null)
 			_instance = new ResourceManager();

		return _instance;
	}

	/**
	 * ResourceManager constructor initializing thread pool and VM list
	 */
	private ResourceManager() {
		this.vmThreadPool = Executors.newCachedThreadPool();
		this.vmList = new LinkedList<>();
		this.vmInstancesRequests = new ArrayList<>();
		interrupt = false;

		logger.info("Initializing the Resource Manager");

		switch (StaticProperties.LEASING_POLICY) {
			case "elastic":
				/* Create a dynamic provisioning policy, will require dynamically require changes in VM number */

				logger.info("The Resource Manager has selected an ELASTIC LEASING policy");

				leasingPolicy = new SimpleVMLeasingPolicy(
						StaticProperties.LEASING_THRESHOLD,
						StaticProperties.LEASING_DELTA
				);
				break;

			/* Choose a default static policy */
			default:
				logger.info("The Resource Manager has selected a STATIC LEASING policy");

				/* Create a static provisioning policy, no change in the number of VMs ever */
				leasingPolicy = new SimpleVMLeasingPolicy();
		}

		/* Currently only one releasing strategy available */
		switch (StaticProperties.RELEASING_POLICY) {
			case "simple":
			default:
				logger.info("The Resource Manager has selected a SIMPLE RELEASING policy");

				releasingPolicy = new SimpleVMReleasingPolicy();
		}

		logger.info("The Resource Manager is creating its initial VMs...");
		/* Create as many VMs as initially required */
		for (int i = 0; i < StaticProperties.MIN_VMS; ++i) {
			allocateVM();

			logger.info("The Resource Manger has requested the creation of a VM...");
		}
	}

	/**
	 * Runs a virtual machine on one of the threads
	 */
	private synchronized VMHandle allocateVM() {
		VMHandle handle = new VMHandle();

		vmInstancesRequests.add(vmThreadPool.submit(handle));

		return handle;
	}
	
	/**
	 * adds an active VMInstance to the list of available VMs
	 * @param vm the VM instance to be added
	 */
	public void addVM(VMInstance vm) {
		synchronized (vmList) {
			vmList.add(vm);
			vmList.notifyAll();
			
			logger.info(String.format(
					"VM#%s has been added to the resource pool.",
					vm.getId())
			);
		}
	}

	public void removeVM(VMInstance vm) {
		synchronized (vmList) {
			vmList.remove(vm);
			/* Notification is not really necessary here, since other threads are
			* awaiting on new VMs being added, rather than being removed, but whatever.
			*/
			vmList.notifyAll();

			logger.info(String.format(
					"VM#%s has been removed from the resource pool.",
					vm.getId())
			);
		}
	}


	public void setInterrupt(boolean interrupt) {
		this.interrupt = interrupt;
	}


	/**
	 * Updates the VM resources available to the application
	 */
	public void run() {
		/*  Holds a list of the VMs which are currently being allocated, i.e. in a pending state. This list is not thread safe. It should not be exposed. */
		List<VMHandle> allocationRequests = new ArrayList<>();

		while(!interrupt) {

			logger.info("Starting a new VM provisioning run...");

			logger.info("Removing VM allocations which have finished");
			/* Remove those handles that have finished (either successfully or not) */
			allocationRequests.removeIf(currentHandle -> !currentHandle.getHandleState().equals(VMHandleState.PENDING));

			logger.info("Computing the number of new VM instances required...");
			int adjustment = leasingPolicy.requiresAdjustment(
					vmList,
					allocationRequests,
					JobManager.instance().getPendingJobQueueSize(),
					JobManager.instance().getRunningJobQueueSize()
			);

			logger.info(
					String.format(
							"There is need of %d more/less VMs",
							adjustment
					)
			);

			if (adjustment == 0)
				logger.info("No adjustment needed");
			else if (adjustment > 0) {
				logger.info("New VMs will be requested");

				/* Request the allocation of new VMs, and save up their requests */
				for (int i = 0; i < adjustment; ++i)
					allocationRequests.add(allocateVM());

				logger.info("New VMs have been requested");

			} else {
				logger.info("Some of the existing VMs will be removed");

				releasingPolicy.removeVMs(-adjustment, vmList);

				logger.info("Some of the existing VMs have been removed");
			}

			logger.info("Going back to sleep. Resource Manager will wake up in 10 seconds.");


			// Notify everyone waiting on the VMs
			synchronized (vmList) {
				vmList.notifyAll();
			}

			try {
				Thread.sleep(WAIT_TIME);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			logger.info("Woken up. Resource manager will reiterate...");
		}

		logger.info("The Resource Manager thread has been interrupted. Will begin cleanup.");

		cleanup();

		logger.info("The Resource Manager thread has finished cleanup. Will finish execution now...");
	}

	/**
	 * Thread cleanup
	 */
	private void cleanup() {
//

//		logger.info("Attempting to remove all the pending VM instance requests.");
//		try {
//			if (vmThreadPool.awaitTermination(30, TimeUnit.SECONDS))
//				logger.info("All jobs terminated successfully");
//			else {
//				/* Start off by halting currently running VM allocations */
//				for (Future<?> instanceReq : vmInstancesRequests)
//					/*
//					* This will interrupt those threads which are currently running a request for VM instantiation.
//					* Those that have already finished this will not be affected by the interruption.
//					*/
//					instanceReq.cancel(true);
//
//				logger.info("Not all VM requests finished their execution. They've been interrupted. Will wait for them to finish.");
//
//				if (!vmThreadPool.awaitTermination(30, TimeUnit.SECONDS)) {
//					vmThreadPool.shutdownNow();
//
//					logger.info(
//							"VM requests were still not able to finish in time. " +
//							"They have been forcefully shut down. Please manually " +
//							"remove running resource from the cloud provider."
//					);
//				}
//			}
//
//		} catch (InterruptedException e) {
//			/* If interrupted kill all jobs now */
//			vmThreadPool.shutdownNow();
//
//			logger.error("The JobManager was interrupted whilst waiting for the jobs to finish. All jobs will be terminated now, regardless");
//		}
//		logger.info("Pending VM instance requests have been removed.");
//
//		logger.info("Attempting to remove all running VM instances.");
//		for (VMInstance vmInstance : vmList)
//			vmInstance.terminateThisInstance();
//
//		logger.info("Running VM instances have been removed");

		for (VMInstance vmInstance : vmList) {
			vmInstance.terminateThisInstance();
		}
	}

	/**
	 * Select a VM instance which is currently available, based on a given policy
	 *
	 * @param vmSelectionPolicy the VM selection policy
	 * @param job the job previously selected
	 * @return
	 */
	public VMInstance getAvailableVM(VMSelectionPolicy vmSelectionPolicy, Job job) {

		return vmSelectionPolicy.selectVM(vmList, job);

	}

	/**
	 * Entering thread will wait on the private field vmList. The thread should be awoken when a VM becomes idle
	 *
	 * @throws InterruptedException when the thread is interrupted whilst waiting
	 */
    public void waitOnVMs() throws InterruptedException {

		synchronized (vmList) {
			vmList.wait();
		}

    }

	public void printVMs() {

		for (VMInstance vmInstance : vmList) {
			System.out.println(vmInstance.toString());
		}

	}

	public void updateStats() {
		for (VMInstance vmInstance : vmList)
			vmInstance.updateStats();

	}
}
