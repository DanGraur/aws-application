package server.resource;

public class VMStartupException extends Exception {

    public VMStartupException() {
        super("Could not start the VM instance");
    }

    public VMStartupException(String message) {
        super(message);
    }

    public VMStartupException(String message, Throwable cause) {
        super(message, cause);
    }

    public VMStartupException(Throwable cause) {
        super(cause);
    }

    public VMStartupException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
