package server.client;

import java.io.Serializable;
import java.util.UUID;

/**
 * The JobRequest holds all information needed from the Client
 * to properly create Jobs on the Server. A JobRequest can be 
 * used to initialize a Job
 * @author Jeff
 */
public class JobRequest implements Serializable {
	private static final long serialVersionUID = -5399605122490343339L;

	/**
	 * Path to file to be converted
	 */
	private String request;
	/**
	 * The unique ID of the Request
	 */
	private int id;
	
	public JobRequest(String request, int id) {
		this.request = request;
		this.id = id;
	}

	/**
	 * Get the name of the file which will be required for conversion
	 *
	 * @return the name of the file required for conversion
	 */
	public String getRequest() {
		return request;
	}
	
	public void setRequest(String req) {
		this.request = req;
	}
	
	public int getId() {
		return id;
	}
}
