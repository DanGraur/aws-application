package server.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;

import server.schedule.Job;
import server.schedule.JobManager;

/**
 * The ClientListener listens to the Client JobRequests
 * and adds them as Jobs to the JobManager pendingQueue.
 * @author Jeff
 */
public class ClientListener extends Thread {
	private int port;
	private ServerSocket serverSocket;
	
	public ClientListener(int port) {
		super();
		
		this.port = port;
		this.serverSocket = null;
	}
	
	/**
	 * The ClientListener can be run to establish a Server,
	 * await a Client, and process all incoming JobRequests.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		try {
			/* Create the Server Socket */
			this.serverSocket = new ServerSocket(this.port);

	        while (true) {    
	            /* Create the Client Socket */
	            Socket clientSocket = serverSocket.accept();
	            System.out.println("Socket Established...");
	            
	            /* Create input and output streams to Client */
	            ObjectOutputStream outToClient = 
	            		new ObjectOutputStream(clientSocket.getOutputStream());
	            ObjectInputStream inFromClient = 
	            		new ObjectInputStream(clientSocket.getInputStream());

	            /* Create JobRequest list and retrieve information */
	            LinkedList<JobRequest> inList = new LinkedList<>();
	            inList = (LinkedList<JobRequest>) inFromClient.readObject();

	            /* Send modified message back to Client */
	            String msg = inList.size() + " Jobs recieved";
	            outToClient.writeObject(msg); 
	            
	            /* Add each JobRequest to JobManager pendingQueue */
	            while(inList.size() > 0) {
		            JobRequest request = inList.pop();
		            msg = request.getRequest() + " ready";
		            System.out.println(msg);
		            Job job = new Job(Job.generateJobID(), request);
		            JobManager.instance().addPendingJob(job);
	            }
	            
	            /* Echo back each additional client input */
	            String inputLine;
	            while ((inputLine = (String) inFromClient.readObject()) != null) {
	                System.out.println("client: " + inputLine);
	                outToClient.writeObject(inputLine); 
	            }
	        }

	    } catch (Exception e) {
	        System.err.println("Server Error: " + e.getMessage());
	        System.err.println("Localized: " + e.getLocalizedMessage());
	        System.err.println("Stack Trace: " + e.getStackTrace());
	        System.err.println("To String: " + e.toString());
	    } finally {
			String msg = "Finalizing JobListener...";
			System.out.println(msg);
			
			/* close the Server Socket */
			try {
				this.serverSocket.close();
			} catch (IOException e) {
				msg = "IO Exception while closing the server socket: " + e.getMessage();
				System.out.println(msg);
			}
			
			msg = "JobListener has been finalized.";
			System.out.println(msg);
		}
	}
}
