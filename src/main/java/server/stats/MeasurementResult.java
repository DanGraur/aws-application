package server.stats;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Dan Graur 11/1/2017
 */
public class MeasurementResult {

    private String name;
    private long min;
    private long max;
    private double average;

    private List<Long> measurements;

    public MeasurementResult(String name) {
        this.name = name;

        min = Long.MAX_VALUE;
        max = Long.MIN_VALUE;
        average = Double.MAX_VALUE;

        measurements = new ArrayList<>();
    }

    public void addMeasurement(long measurement) {
        measurements.add(measurement);

        min = min <= measurement ? min : measurement;
        max = max >= measurement ? max : measurement;

        long aux = 0L;

        for (Long m : measurements)
            aux += m;

        average = aux / (double) measurements.size();
    }

    public String getName() {
        return name;
    }

    public long getMin() {
        return min;
    }

    public long getMax() {
        return max;
    }

    public double getAverage() {
        return average;
    }

    public List<Long> getMeasurements() {
        return measurements;
    }

    /**
     * Write the contents of the individualRunResults to a CSV file
     *
     * @param baseFolder the folder in which the CSV will be created
     * @param fileName the name of the CSV to be created and written to
     */
    public static void writeToCSV(String baseFolder, String fileName, MeasurementResult... results) throws IOException {
        /*
        * Make sure that indeed the paths are correctly formatted
        * (slash should work on Windows OS as well).
        */
        baseFolder = baseFolder.charAt(baseFolder.length() - 1) == '/' ? baseFolder : baseFolder + '/';
        fileName = fileName.split("\\.")[0] + ".csv";

        /* Declarations and initializations */
        File destinationFile = new File(baseFolder + fileName);

        /* Create the file */
        destinationFile.createNewFile();

        PrintWriter outputCSV = new PrintWriter(destinationFile);
        StringBuilder fileContents = new StringBuilder();

        fileContents.append("Name,Runs,Min,Max,Average\n");
        System.err.println("Name,Runs,Min,Max,Average");



        for (MeasurementResult result : results) {
            fileContents.append(
                    String.format(
                            "%s,%d,%d,%d,%f\n",
                            result.getName(),
                            result.getMeasurements().size(),
                            result.getMin(),
                            result.getMax(),
                            result.getAverage()
                    )
            );
            System.err.println(
                    String.format(
                            "%s,%d,%d,%d,%f",
                            result.getName(),
                            result.getMeasurements().size(),
                            result.getMin(),
                            result.getMax(),
                            result.getAverage()
                    )
            );
        }

        /* Write and Clean-up */
        outputCSV.write(fileContents.toString());
        outputCSV.flush();
        outputCSV.close();
    }

    @Override
    public String toString() {
        return "MeasurementResult{" +
                "name='" + name + '\'' +
                ", min=" + min +
                ", max=" + max +
                ", average=" + average +
                ", measurements size=" + measurements.size() +
                '}';
    }
}
