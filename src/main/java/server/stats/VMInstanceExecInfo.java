package server.stats;

/**
 * @author Dan Graur 11/1/2017
 */
public class VMInstanceExecInfo {

    private String id;
    private long idleTime;
    private long activeTime;

    public VMInstanceExecInfo(String id) {
        this.id = id;
        this.idleTime = 0L;
        this.activeTime = 0L;
    }

    public VMInstanceExecInfo(String id, long idleTime, long activeTime) {
        this.id = id;
        this.idleTime = idleTime;
        this.activeTime = activeTime;
    }

    public double getRatio() {
        return activeTime / (double) idleTime;
    }

    public String getId() {
        return id;
    }

    public long getIdleTime() {
        return idleTime;
    }

    public long getActiveTime() {
        return activeTime;
    }

    public void setIdleTime(long idleTime) {
        this.idleTime = idleTime;
    }

    public void setActiveTime(long activeTime) {
        this.activeTime = activeTime;
    }
}
