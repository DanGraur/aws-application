package server.stats;

import server.util.StaticProperties;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Dan Graur 11/1/2017
 */
public class Analyzer {

    private static Analyzer instance;

    private String testName;

    /**
     * Stores information on the waiting times of each of the jobs
     */
    private final MeasurementResult waitingTimes;

    /**
     * Stores information on the actual execution times of each of the jobs
     */
    private final MeasurementResult makespanTimes;

    /**
     * The execution times
     */
    private final MeasurementResult executionTimes;

    /**
     * Stores information for each of the VM instances to have existed the amount of time spent executing or overall idle time
     */
    private List<VMInstanceExecInfo> vmInfo;

    private Analyzer() {
        waitingTimes = new MeasurementResult("Waiting times");
        makespanTimes = new MeasurementResult("Makespan times");
        executionTimes = new MeasurementResult("Execution time");
        vmInfo = new ArrayList<>();

        testName = StaticProperties.TEST_NAME;
    }

    public static Analyzer getInstance() {
        if (instance == null)
            instance = new Analyzer();

        return instance;
    }

    public void addWaitingTime(long wait) {
        synchronized (waitingTimes) {
            waitingTimes.addMeasurement(wait);

            waitingTimes.notifyAll();
        }
    }

    public void addMakepan(long makespan) {
        synchronized (makespanTimes) {
            makespanTimes.addMeasurement(makespan);

            makespanTimes.notifyAll();
        }
    }

    public void addExecutionTime(long executionTime) {
        synchronized (executionTimes) {
            executionTimes.addMeasurement(executionTime);

            executionTimes.notifyAll();
        }
    }

    public void addVmInfo(VMInstanceExecInfo info) {
        synchronized (vmInfo) {
            vmInfo.add(info);

            vmInfo.notifyAll();
        }
    }

    public void summarize() {

        int count = 5;
        boolean done = false;

        /* Try multiple times, until it succeeds */
        for (int i =0; i < count && !done; ++i) {

            try {
                MeasurementResult.writeToCSV(
                        StaticProperties.SUMMARY_DIR,
                        testName + "_waiting_makespan",
                        waitingTimes,
                        makespanTimes,
                        executionTimes
                );

                done = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /* Compute the average utilization time */
        double utilizationOverall = 0.0;
        double activeTimeTotal = 0.0;
        double totalTime = 0.0;

        for (VMInstanceExecInfo instance : vmInfo) {
            activeTimeTotal += instance.getActiveTime();
            totalTime += instance.getIdleTime();

            utilizationOverall += instance.getRatio();
        }

        utilizationOverall /= vmInfo.size();

        done = false;

        /* Try multiple times, until it succeeds */
        for (int i =0; i < count && !done; ++i) {

            try {
                PrintWriter out = new PrintWriter(new File(StaticProperties.SUMMARY_DIR + testName + "_vm_info.txt"));

                out.printf(
                        "Normalized before, then averaged: %f\n" +
                                "Summed and normalized after: %f\n",
                        utilizationOverall,
                        activeTimeTotal / totalTime
                );

                System.err.printf(
                        "Normalized before, then averaged: %f\n" +
                                "Summed and normalized after: %f\n",
                        utilizationOverall,
                        activeTimeTotal / totalTime
                );

                out.flush();
                out.close();

                done = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
