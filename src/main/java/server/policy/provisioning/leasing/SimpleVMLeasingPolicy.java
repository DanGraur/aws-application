package server.policy.provisioning.leasing;

import server.resource.VMHandle;
import server.resource.VMInstance;
import server.util.CloudLogger;
import server.util.StaticProperties;

import java.util.List;

/**
 * Simple threshold based provisioning policy, which adapts the number of VMs
 * so that the number of pending and active jobs are always within the range
 * currentVMNumber +/- threshold. This needs to take into consideration the
 * MIN and MAX number of VM instances
 */
public class SimpleVMLeasingPolicy implements LeasingProvisioningPolicy {

    /**
     * The threshold above/below which, new VM will be added/removed
     */
    private int threshold;

    /**
     * The number of additional VMs to be added/removed in addition to those VMs which will
     * be added/removed in order to ensure that the threshold is not surpassed (i.e. one
     * more/less VM will require re-provisioning since the threshold will be surpassed again)
     */
    private int deltaVMs;

    /**
     * This creates a policy which maintains a constant amount of VMs regardless of need
     *
     */
    public SimpleVMLeasingPolicy() {
        threshold = Integer.MAX_VALUE;
        deltaVMs = 0;
    }

    public SimpleVMLeasingPolicy(int threshold) {
        this.threshold = threshold;
        this.deltaVMs = 0;
    }

    public SimpleVMLeasingPolicy(int threshold, int deltaVMs) {
        this.threshold = threshold;
        this.deltaVMs = deltaVMs;
    }

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    @Override
    public int requiresAdjustment(final List<VMInstance> vmInstances,
                                  final List<VMHandle> currentRequests,
                                  int pendingJobs,
                                  int activeJobs) {

        int totalJobs = pendingJobs + activeJobs;
        int nrOfVMs;

        /* Make sure you are getting a read on the number of current instances */
        synchronized (vmInstances) {
            nrOfVMs = vmInstances.size();
        }

        /* Consider current (unfinished) VM requests as valid VMs */
        synchronized (currentRequests) {
            nrOfVMs += currentRequests.size();
        }

        /* Get the difference between the number of jobs and VMs */
        int difference = totalJobs - nrOfVMs;

        /* Number of jobs are in acceptable range of number of VMs */
        if (Math.abs(difference) <= threshold) {
            CloudLogger.getInstance().info(
                    String.format(
                            "The number of VMs (%d) and jobs (%d) are in range of each other. Difference: %d.",
                            nrOfVMs,
                            totalJobs,
                            difference
                    )
            );

            return 0;
        }

        /* We need to add VMs */
        if (difference > 0) {
            int newVMNr = difference - threshold + deltaVMs;

            /* Make sure that by adding new VM instances, the maximal instance number threshold is not exceeded */
            newVMNr = newVMNr + nrOfVMs > StaticProperties.MAX_VMS ? StaticProperties.MAX_VMS - nrOfVMs : newVMNr;

            CloudLogger.getInstance().info(
                    String.format(
                            "The number of VMs (%d) and jobs (%d) are NOT in range of each other. Difference: %d. Adding %d VMs",
                            nrOfVMs,
                            totalJobs,
                            difference,
                            newVMNr
                    )
            );

            return newVMNr;
        }

        /* This number should be negative */
        int newVMNr = difference - threshold + deltaVMs;

        newVMNr = nrOfVMs + newVMNr < StaticProperties.MIN_VMS ? StaticProperties.MIN_VMS - nrOfVMs : newVMNr;

        /* We need to remove VMs */
        CloudLogger.getInstance().info(
                String.format(
                        "The number of VMs (%d) and jobs (%d) are NOT in range of each other. Difference: %d. Removing %d VMs",
                        nrOfVMs,
                        totalJobs,
                        difference,
                        newVMNr
                )
        );
        return newVMNr;
    }

}
