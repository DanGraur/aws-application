package server.policy.provisioning.leasing;

import server.resource.VMHandle;
import server.resource.VMInstance;

import java.util.List;

public interface LeasingProvisioningPolicy {


    /**
     * Method which indicates if the number of VMs needs to be adjusted.
     *
     * @param vmInstances a list of the currently available VMs
     * @param currentRequests the number of current VM allocation requests
     * @param pendingJobs the number of pending jobs
     * @param activeJobs the number of active jobs
     * @return an integer value which specifies how the number of VMs needs to be adjusted. If the value is positive, as many VMs should be added.
     *         If the value is negative, the number of VMs needs to be reduced with the specified amount. If the number is 0 there is no need
     *         to change the available resources.
     */
    int requiresAdjustment(List<VMInstance> vmInstances, List<VMHandle> currentRequests, int pendingJobs, int activeJobs);

}
