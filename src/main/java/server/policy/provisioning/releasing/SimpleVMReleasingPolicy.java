package server.policy.provisioning.releasing;

import server.resource.VMInstance;
import server.util.CloudLogger;

import java.util.Iterator;
import java.util.List;

public class SimpleVMReleasingPolicy implements ReleasingProvisioningPolicy {

    /**
     * Remove those VMs which are currently idle.
     *
     * @param nrOfRemovedVMs the number of VMs to be removed
     * @param vmInstances the list of currently available instances
     */
    @Override
    public void removeVMs(int nrOfRemovedVMs, final List<VMInstance> vmInstances) {

        synchronized (vmInstances) {
            Iterator<VMInstance> iterator = vmInstances.iterator();

            /* Should always be positive, but just in case... */
            nrOfRemovedVMs = Math.abs(nrOfRemovedVMs);

            while (iterator.hasNext() && nrOfRemovedVMs > 0) {
                final VMInstance vm = iterator.next();

                    if (!vm.isBusy()) {
                        CloudLogger.getInstance().info(
                                String.format(
                                        "Removing VM instance %s...",
                                        vm.getId()
                                )
                        );

                        /* Then remove it from the list of VMs */
                        iterator.remove();

                        /* First terminate the instance */
                        vm.terminateThisInstance();

                        CloudLogger.getInstance().info(
                                String.format(
                                        "VM instance %s has been removed successfully",
                                        vm.getId()
                                )
                        );

                        --nrOfRemovedVMs;

                }
            }
        }
    }
}
