package server.policy.provisioning.releasing;

import server.resource.VMInstance;

import java.util.List;

public interface ReleasingProvisioningPolicy {

    /**
     * Method which will decide which VMs to remove.
     *
     * @param nrOfRemovedVMs the number of VMs to be removed
     * @param vmInstances the list of currently available instances
     */
    void removeVMs(int nrOfRemovedVMs, List<VMInstance> vmInstances);
}
