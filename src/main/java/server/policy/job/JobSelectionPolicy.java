package server.policy.job;

import server.schedule.Job;
import server.schedule.JobQueue;

/**
 * Interface to a job selection policy
 *
 */
public interface JobSelectionPolicy {

    /**
     * Should implement a job selection policy. The job should be removed from the storing collection
     *
     * @param jobs the collection of currently pending jobs
     */
    public Job selectJob(JobQueue jobs);

}
