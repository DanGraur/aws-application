package server.policy.job;

import server.schedule.Job;
import server.schedule.JobQueue;

import java.util.List;

/**
 * Implements a simple job selection policy for the job which has been waiting for the longest time
 *
 */
public class OldestJobSelectionPolicy implements JobSelectionPolicy {

    @Override
    public Job selectJob(JobQueue jobs) {
        return jobs.popJob();
    }

}
