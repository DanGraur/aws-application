package server.policy.vm;

import server.resource.VMInstance;
import server.resource.VMState;
import server.schedule.Job;

import java.util.Iterator;
import java.util.List;

/**
 * Policy which simply selects the first VM it encounters that is free for execution
 * It does not take into consideration the job previously selected
 *
 */
public class FirstAvailableVMPolicy implements VMSelectionPolicy {

    /**
     * This policy will select the first VM which is idle (avaailable for computation)
     *
     * @param vmInstances the list of VM instances currently running
     * @param selectedJob the job selected for executing on a VM
     *
     * @return the selected instance or null
     */
    @Override
    public VMInstance selectVM(final List<VMInstance> vmInstances, Job selectedJob) {
        boolean marker = true;
        VMInstance selectedInstance = null;

        synchronized (vmInstances) {
            Iterator<VMInstance> iterator = vmInstances.iterator();

            while(iterator.hasNext() && marker) {
                VMInstance currentInstance = iterator.next();

                //System.out.println("Set VM: " + currentInstance.toString());

                /* Make sure that the selected VM is both available, and running */
                if (!currentInstance.isBusy() && currentInstance.getTrueState().equals(VMState.RUNNING)) {
                    selectedInstance = currentInstance;

                    marker = false;
                }
            }

            if (selectedInstance != null)
                selectedInstance.setBusy(true);

            /* Wake up the threads waiting on this */
            vmInstances.notifyAll();
        }

        return selectedInstance;
    }

}
