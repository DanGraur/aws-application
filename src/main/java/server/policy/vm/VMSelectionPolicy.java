package server.policy.vm;

import server.resource.VMInstance;
import server.schedule.Job;

import java.util.List;

/**
 * Interface for selection policies which do take into consideration the task which has been selected.
 * Examples include: selection policy which pairs up the job with a VM suitable for its size, or for its
 * specific characteristics
 */
public interface VMSelectionPolicy {

    /**
     * Select a VM, optionally based on the characteristics of a job
     *
     * @param vmInstances the list of VM instances currently running
     * @param selectedJob the job selected for executing on a VM
     * @return
     */
    VMInstance selectVM(List<VMInstance> vmInstances, Job selectedJob);

}
