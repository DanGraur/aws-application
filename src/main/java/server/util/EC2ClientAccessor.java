package server.util;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.InstanceState;

/**
 * Class implementing a variation of the singleton pattern on the EC2 Client
 */
public class EC2ClientAccessor {

    private static AmazonEC2Client client;

    private  EC2ClientAccessor() {
    }

    public static AmazonEC2Client getInstance() {

        if (client == null) {

            client = new AmazonEC2Client(
                    new BasicAWSCredentials(
                            StaticProperties.ACCESS_KEY,
                            StaticProperties.PRIVATE_ACCESS_KEY
                    )
            );

            client.setEndpoint(Region.getRegion(Regions.US_EAST_1).getServiceEndpoint("ec2"));
            //client.setRegion(Region.getRegion(Regions.US_EAST_1));
        }

        return client;
    }

    public static Integer getInstanceStatus(String instanceId) {

        DescribeInstancesRequest describeInstancesRequest = new DescribeInstancesRequest().withInstanceIds(instanceId);
        DescribeInstancesResult describeInstancesResult = getInstance().describeInstances(describeInstancesRequest);
        InstanceState state = describeInstancesResult.getReservations().get(0).getInstances().get(0).getState();

        return state.getCode();
    }

    public static String getInstanceIp(String instanceId) {

        DescribeInstancesRequest describeInstancesRequest = new DescribeInstancesRequest().withInstanceIds(instanceId);
        DescribeInstancesResult describeInstancesResult = getInstance().describeInstances(describeInstancesRequest);
        String ipAddress = describeInstancesResult.getReservations().get(0).getInstances().get(0).getPublicIpAddress();

        return ipAddress;
    }
}
