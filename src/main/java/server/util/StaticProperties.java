package server.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Class containing the static configurations for creating EC2 instances and establishing SSH connections with these instances
 *
 */
public class StaticProperties {

    public final static String BASE_DIR_REMOTE;
    public final static String BASE_DIR_LOCAL;

    public final static String VM_USERNAME;
    public final static String PASSWORD;
    public final static boolean TRUST_ENTITIES;
    public final static String KEYPAIR_LOCAITON;

    public final static String KEYPAIR_NAME;
    public final static String ACCESS_KEY;
    public final static String PRIVATE_ACCESS_KEY;
    public final static String ACCESS_GROUP;
    public final static String IMG_TYPE;
    public final static String VM_TYPE;

    public final static int MIN_INSTANCES = 1;
    public final static int MAX_INSTANCES = 1;

    public final static String SOURCE_TYPE;
    public final static String TARGET_TYPE;

    public final static String LEASING_POLICY;
    public final static int MIN_VMS;
    public final static int MAX_VMS;
    public final static int LEASING_DELTA;
    public final static int LEASING_THRESHOLD;
    public final static String RELEASING_POLICY;

    public static final String TEST_NAME;
    public static final int TOTAL_JOBS;
    public static final String SUMMARY_DIR;
    public static final String CONFIGURATION_DIR;
    public static final String TEST_CONFIGURATION;

    public static final String CACHE_DIR;

    static {
        Properties prop = new Properties();

        try {
            prop.load(
                    new FileInputStream("src/main/resources/app.properties")
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        BASE_DIR_REMOTE = prop.getProperty("video.location.remote");
        BASE_DIR_LOCAL = prop.getProperty("video.location.local");

        VM_USERNAME = prop.getProperty("ssh.username");
        PASSWORD = prop.getProperty("ssh.password");
        TRUST_ENTITIES = prop.getProperty("ssh.trust_unknown_entities").equals("true");
        KEYPAIR_LOCAITON = prop.getProperty("ssh.keypair.location");

        KEYPAIR_NAME = prop.getProperty("ec2.keypair.name");
        ACCESS_KEY = prop.getProperty("ec2.access_key");
        PRIVATE_ACCESS_KEY = prop.getProperty("ec2.secret_access_key");
        IMG_TYPE = prop.getProperty("ec2.image.type");
        VM_TYPE = prop.getProperty("ec.instance.type");
        ACCESS_GROUP = prop.getProperty("ec2.access_group");

        SOURCE_TYPE = prop.getProperty("video.source.type");
        TARGET_TYPE = prop.getProperty("video.target.type");

        LEASING_POLICY = prop.getProperty("vm.provisioning.leasing.policy");
        MIN_VMS = Integer.parseInt(prop.getProperty("vm.provisioning.min_vms"));
        MAX_VMS = Integer.parseInt(prop.getProperty("vm.provisioning.max_vms"));
        LEASING_DELTA = Integer.parseInt(prop.getProperty("vm.provisioning.leasing.delta"));
        LEASING_THRESHOLD = Integer.parseInt(prop.getProperty("vm.provisioning.leasing.threshold"));
        RELEASING_POLICY = prop.getProperty("vm.provisioning.releasing.policy");

        TEST_NAME = prop.getProperty("test.name");
        TOTAL_JOBS = Integer.parseInt(prop.getProperty("test.jobs"));
        SUMMARY_DIR = prop.getProperty("test.save.dir");
        CONFIGURATION_DIR = prop.getProperty("test.configuration.dir");
        TEST_CONFIGURATION = prop.getProperty("test.configuration.file");

        CACHE_DIR = prop.getProperty("cache.destination");

        // Let the gc deal with this
        prop = null;
    }
}
