package server.util;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.util.Properties;

public class CloudLogger {
    private static Logger ourInstance;

    /**
     * Get the application's global logger
     *
     * @return the application's global logeer
     */
    public static Logger getInstance() {
        if (ourInstance == null)
            ourInstance = Logger.getLogger("IN4392");

        return ourInstance;
    }

    /**
     * Create and return a job logger
     *
     * @param jobId the id of the job whose logger is being created
     * @return a logger belonging to the specified job
     */
    public static Logger createJobLogger(int jobId) {

        String loggerName = "job" + jobId;
        String logPath = "log/job/job" + jobId + ".log";
        String appenderName = "Job_Log_" + jobId;

        String fullAppenderName = "log4j.appender." + appenderName + '.';

        Properties properties = new Properties();

        properties.setProperty("log4j.logger." + loggerName, "DEBUG, " + appenderName);
        properties.setProperty(fullAppenderName.substring(0, fullAppenderName.length() - 1), "org.apache.log4j.RollingFileAppender");
        properties.setProperty(fullAppenderName + "File", logPath);
        properties.setProperty(fullAppenderName + "MaxFileSize", "10MB");
        properties.setProperty(fullAppenderName + "MaxBackupIndex", "10");
        properties.setProperty(fullAppenderName + "layout", "org.apache.log4j.PatternLayout");
        properties.setProperty(fullAppenderName + "layout.ConversionPattern", "%d{ISO8601} %-5p (%t) [%c{1}(%M:%L)] %m%n");

        PropertyConfigurator.configure(properties);

        return Logger.getLogger(loggerName);
    }

    /**
     * Create and return a logger belonging to a new VM instance
     *
     * @param instanceID the id of the VM instance whose logger is being created
     * @return a logger belonging to the specified VM
     */
    public static Logger createInstanceLogger(String instanceID) {

        String loggerName = "instance" + instanceID;
        String logPath = "log/instance/instance" + instanceID + ".log";
        String appenderName = "Instance_Log_" + instanceID;

        String fullAppenderName = "log4j.appender." + appenderName + '.';

        Properties properties = new Properties();

        properties.setProperty("log4j.logger." + loggerName, "DEBUG, " + appenderName);
        properties.setProperty(fullAppenderName.substring(0, fullAppenderName.length() - 1), "org.apache.log4j.RollingFileAppender");
        properties.setProperty(fullAppenderName + "File", logPath);
        properties.setProperty(fullAppenderName + "MaxFileSize", "10MB");
        properties.setProperty(fullAppenderName + "MaxBackupIndex", "10");
        properties.setProperty(fullAppenderName + "layout", "org.apache.log4j.PatternLayout");
        properties.setProperty(fullAppenderName + "layout.ConversionPattern", "%d{ISO8601} %-5p (%t) [%c{1}(%M:%L)] %m%n");

        PropertyConfigurator.configure(properties);

        return Logger.getLogger(loggerName);
    }
}
