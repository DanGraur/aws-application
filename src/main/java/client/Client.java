package client;

import java.io.*;
import java.net.*;
import java.util.LinkedList;

import server.client.JobRequest;

/**
 * The Client for IN4392-Lab.
 * @author Jeff
 */
public class Client {
	
	/**
	 * The Client connects to the Server via localhost on port 4392, 
	 * sending a list of JobRequest to be processed. The different 
	 * client modes are:
	 * 	- testThreading
	 * 	- testEcho
	 * 
	 * Note: The Server must be run first.
	 * 
	 * @param args
	 * @throws IOException
	 */
    public static void main(String[] args) throws IOException {
       testThreading();
    }
    
    /**
     * testJob() creates and sends JobRequest objects to the Server.
     */
	public static void testThreading() {
		String hostName = "localhost";
		int portNumber = 4392;
		Socket clientSocket = null;
		
	    try {
	    		/* Create client Socket and connect to localhost Server */
	    		clientSocket = new Socket(hostName, portNumber);
	        
	    		/* Create streams to send JobRequest objects to Server */
	        ObjectOutputStream outToServer = 
	        		new ObjectOutputStream(clientSocket.getOutputStream());
	        ObjectInputStream inFromServer = 
	        		new ObjectInputStream(clientSocket.getInputStream());
	
	        /* Create list of JobRequest */
	        LinkedList<JobRequest> outList = new LinkedList<>();
	        JobRequest request = null;
	        request = new JobRequest("Job 1", 1);
	        outList.add(request);
	        request = new JobRequest("Job 2", 2);
	        outList.add(request);
	        
	        /* Send the LinkedList<JobRequest> Object to the Server */
	        outToServer.writeObject(outList);
	        
	        /* Retrieve message from Server */
	        String msgFrmServer = (String) inFromServer.readObject();
	        System.out.println("Server: " + msgFrmServer);
	
	        /* Create BufferedReader to send instruction to Server */
	        BufferedReader stdIn =
	                new BufferedReader(
	                    new InputStreamReader(System.in));
	        
	        /* Send userInput from terminal to Server */
	        String userInput;
	        while ((userInput = stdIn.readLine()) != null) {
	            outToServer.writeObject(userInput);
	            
	            /* Print out messages received from Server */
	            msgFrmServer = (String) inFromServer.readObject();
		        System.out.println("Server: " + msgFrmServer);
	        }
	        
	        /* Close connection when done */
	        clientSocket.close();
	        
	    } catch (Exception e) {
	    		System.err.println("Client Error: " + e.getMessage());
	        System.err.println("Localized: " + e.getLocalizedMessage());
	        System.err.println("Stack Trace: " + e.getStackTrace());
	    }
	}
    
	/**
	 * testEcho() sends inputLines from ternminal to Server
	 * and the Server echos back the user input to the Client.
	 */
    public void testEcho() {
    		String hostName = "localhost";
        int portNumber = 4392;
        
    		try (
    			/* Create client Socket, then connect to Server */
            Socket echoSocket = new Socket(hostName, portNumber);
    				
    			/* Create inputs and outputs, and inputLine to Server */
            PrintWriter out =
                new PrintWriter(echoSocket.getOutputStream(), true);
            BufferedReader in =
                new BufferedReader(
                    new InputStreamReader(echoSocket.getInputStream()));
            BufferedReader stdIn =
                new BufferedReader(
                    new InputStreamReader(System.in))
        ) {
    			/* Send each userInput to Server*/
            String userInput;
            while ((userInput = stdIn.readLine()) != null) {
                out.println(userInput);
                
                /* Print out reponse from Server */
                System.out.println("server: " + in.readLine());
            }
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + hostName);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " +
                hostName);
            System.exit(1);
        } 
    }
    
    
}

