package server.statistics;

import java.util.HashMap;

/**
 * Class for adding times for periods: etc idle time.
 * @author Jeff
 *
 */
public class StopWatch {

	private HashMap<String, Long> markMap;
	private HashMap<String, Long> stopWatchMap;


	/**
	 * Constructor.
	 */
	public StopWatch() {
		this.markMap = new HashMap<String, Long>();
		this.stopWatchMap = new HashMap<String, Long>();
	}

	public void clear() {
		this.markMap.clear();
		this.stopWatchMap.clear();
	}

	public Long getMark(String key) {
		return this.markMap.get(key);
	}

	/**
	 * Gets a value from the statistics map with a given key.
	 * @return The period of the given key if exists, null if not.
	 */
	public Long getPeriod(String key) {
		return this.stopWatchMap.get(key);
	}

	/**
	 * Clears all marks.
	 */
	public void clearMarks() {
		this.markMap.clear();
	}

	/**
	 * Clears all statistics.
	 */
	public void clearTimes() {
		this.stopWatchMap.clear();
	}

	/**
	 * Clears all.
	 */
	public void clearAll() {
		clearMarks();
		clearTimes();
	}

	/**
	 * Set a mark with a given name.
	 * @param key The name of this mark.
	 */
	public void mark(String key) {
		long thisTime = System.currentTimeMillis();

		/*
		 * if the mark exists, remove it and aggregate this period into the
		 * statistics map.
		 */
		if (this.markMap.containsKey(key)) {
			long start = this.markMap.remove(key);
			long period = thisTime - start;
			if (this.stopWatchMap.containsKey(key)) {
				period += this.stopWatchMap.remove(key);
			}
			this.stopWatchMap.put(key, period);
		}
		/*
		 * if the mark doesn't exist, just add it into the mark map.
		 */
		else {
			this.markMap.put(key, thisTime);
		}
	}
	
}
