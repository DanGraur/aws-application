package server.statistics;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;

import server.resource.VMInstance;
import server.schedule.Job;

/**
 * The StatisticsManager is a singleton responsible for the 
 * experiment and system analysis. It collects, processes, and 
 * outputs all relevant job, VM, and simulation statistics to a
 * file.
 * @author Jeff
 *
 */
public class StatisticsManager {

	/* the only instance */
	private final static StatisticsManager _instance = new StatisticsManager();

	/* maintains statistics of all finished jobs */
	private HashMap<Integer, HashMap<String, Long>> jobMap;
	private HashMap<Integer, HashMap<String, Long>> failedJobMap;
	
	/* for calculating VM performance time */
	private HashMap<Integer, HashMap<String, Long>> vmMap;

	/* maintains system statistics */
	private HashMap<String, Long> systemMap;
	
	/* maintains system performance over time */
	private LinkedList<HashMap<String, Long>> systemList;

	public StatisticsManager() {

		this.initializeSystemMap();
	}

	private void initializeSystemMap() {
		long value = 0;
		this.systemMap.put("JobsAccepted", value);
		this.systemMap.put("JobsFinished", value);
		this.systemMap.put("JobsFailures", value);

		this.systemMap.put("VMsAllocated", value);
		this.systemMap.put("VMsFinalized", value);
		this.systemMap.put("MaximumExistingVMs", value);

		this.systemMap.put("VMAllocationAttempts", value);
		this.systemMap.put("VMAllocationFailures", value);
	}

	/**
	 * Public interface for getting the only instance.
	 * @return The StatisticsManager instance.
	 */
	public static StatisticsManager instance() {
		return _instance;
	}

	public void recordJob(Job job) {
		this.jobMap.put(job.getId(), job.summarize());
		this.addFinishedJob();
	}
	
	public void recordVM(VMInstance vm) {
		synchronized (this.vmMap) {
			this.vmMap.put(vm.getId(), vm.summarize());
		}
	}

	public void recordFailedJob(Job job) {
		this.failedJobMap.put(job.getId(), job.summarize());
	}

	public void addSystemStatistics(HashMap<String, Long> data) {
		synchronized (this.systemList) {
			this.systemList.add(data);
		}
	}

	public void addAcceptedJob() {
		synchronized (this.systemMap) {
			long value = this.systemMap.remove("JobsAccepted");
			this.systemMap.put("JobsAccepted", value + 1);
		}
	}

	public void addFinishedJob() {
		synchronized (this.systemMap) {
			long value = this.systemMap.remove("JobsFinished");
			this.systemMap.put("JobsFinished", value + 1);
		}
	}

	public void addJobsFailure() {
		synchronized (this.systemMap) {
			long value = this.systemMap.remove("JobsFailures");
			this.systemMap.put("JobsFailures", value + 1);
		}
	}

	public void addAllocatedVM() {
		synchronized (this.systemMap) {
			long value = this.systemMap.remove("VMsAllocated");
			this.systemMap.put("VMsAllocated", value + 1);
		}
	}

	public void addFinalizedVM() {
		synchronized (this.systemMap) {
			long value = this.systemMap.remove("VMsFinalized");
			this.systemMap.put("VMsFinalized", value + 1);
		}
	}

	public void addVMAllocationAttempts() {
		synchronized (this.systemMap) {
			long value = this.systemMap.remove("VMAllocationAttempts");
			this.systemMap.put("VMAllocationAttempts", value + 1);
		}
	}

	public void addVMAllocationFailures() {
		synchronized (this.systemMap) {
			long value = this.systemMap.remove("VMAllocationFailures");
			this.systemMap.put("VMAllocationFailures", value + 1);
		}
	}

	public void updateMaximumExistingVMs(long vms) {
		synchronized (this.systemMap) {
			long value = this.systemMap.remove("MaximumExistingVMs");
			value = value > vms ? value : vms;
			this.systemMap.put("MaximumExistingVMs", value);
		}
	}
	
	public void saveToFile(String outFilePath) throws IOException {
		BufferedWriter writer = new BufferedWriter(
				new FileWriter(outFilePath));

		String content = "";
		
		/* system statistics */
		writer.write("\n====================\nSystem Statistics\n====================\n");
		content = String.format("Jobs accepted: %d\n", systemMap.get("JobsAccepted"));
		content += String.format("Jobs finished: %d\n", systemMap.get("JobsFinished"));
		content += String.format("Jobs failures: %d\n", systemMap.get("JobsFailures"));

		content += String.format("VMs allocated: %d\n", systemMap.get("VMsAllocated"));
		content += String.format("VMs finalized: %d\n", systemMap.get("VMsFinalized"));
		content += String.format("Maximum existing VMs: %d\n", systemMap.get("MaximumExistingVMs"));

		content += String.format("VM allocation attempts: %d\n", systemMap.get("VMAllocationAttempts"));
		content += String.format("VM allocation failures: %d\n", systemMap.get("VMAllocationFailures"));

		writer.write(content);
		
		/* overall job statistics */
		writer.write("\n====================\nOverall Job Statistics\n====================\n");

		Long[] totalTime = createStats();
		Long[] waitTime = createStats();
		Long[] runningTime = createStats();
		Long[] uploadTime = createStats();
		Long[] tarballExtractionTime = createStats();
		Long[] executionTime = createStats();
		Long[] downloadTime = createStats();

		long totalNumber = 0;

		Iterator<Entry<Integer, HashMap<String, Long>>> itr2 = this.jobMap.entrySet().iterator();
		while (itr2.hasNext()) {
			Entry<Integer, HashMap<String, Long>> entry = itr2.next();
			HashMap<String, Long> data = entry.getValue();

			updateStats(totalTime, data.get("totalTime"));
			updateStats(waitTime, data.get("waitTime"));
			updateStats(runningTime, data.get("runningTime"));
			updateStats(uploadTime, data.get("uploadTime"));
			updateStats(tarballExtractionTime, data.get("tarballExtractionTime"));
			updateStats(executionTime, data.get("executionTime"));
			updateStats(downloadTime, data.get("downloadTime"));

			totalNumber++;
		}
		if (totalNumber != 0) {
			content = String.format("totalTime = %s sec\n", formatStats(totalTime, totalNumber));
			content += String.format("waitTime = %s sec\n", formatStats(waitTime, totalNumber));
			content += String.format("runningTime = %s sec\n", formatStats(runningTime, totalNumber));
			content += String.format("uploadTime = %s sec\n", formatStats(uploadTime, totalNumber));
			content += String.format("tarballExtractionTime = %s sec\n", formatStats(tarballExtractionTime, totalNumber));
			content += String.format("executionTime = %s sec\n", formatStats(executionTime, totalNumber));
			content += String.format("downloadTime = %s sec\n", formatStats(downloadTime, totalNumber));
		}
		else {
			content = "null\n";
		}
		writer.write(content);
		
		/* overall job statistics */
		writer.write("\n====================\nOverall VM Statistics\n====================\n");

		Long[] lifeTime = createStats();
		Long[] bootingTime = createStats();
		Long[] preperationTime = createStats();
		Long[] idleTime = createStats();
		Long[] busyTime = createStats();

		long totalVM = 0;
		
		Iterator<Entry<Integer, HashMap<String, Long>>> itr3 = this.vmMap.entrySet().iterator();
		while (itr3.hasNext()) {
			Entry<Integer, HashMap<String, Long>> entry = itr3.next();
			HashMap<String, Long> data = entry.getValue();

			updateStats(totalTime, data.get("lifeTime"));
			updateStats(waitTime, data.get("bootingTime"));
			updateStats(runningTime, data.get("preperationTime"));
			updateStats(uploadTime, data.get("idleTime"));
			updateStats(tarballExtractionTime, data.get("busyTime"));

			totalVM++;
		}
		if (totalVM != 0) {
			content = String.format("lifeTime = %s sec\n", formatStats(lifeTime, totalVM));
			content += String.format("bootingTime = %s sec\n", formatStats(bootingTime, totalVM));
			content += String.format("preperationTime = %s sec\n", formatStats(preperationTime, totalVM));
			content += String.format("idleTime = %s sec\n", formatStats(idleTime, totalVM));
			content += String.format("busyTime = %s sec\n", formatStats(busyTime, totalVM));
			
		}
		else {
			content = "null\n";
		}
		
		writer.write(content);
		
		/* detailed job statistics */
		writer.write("\n====================\nJob Statistics\n====================\n");
		writer.write("#id arrivalTime failures makespan waitTime runningTime uploadTime tarballExtractionTime executionTime downloadTime\n");
		itr2 = this.jobMap.entrySet().iterator();
		while (itr2.hasNext()) {
			Entry<Integer, HashMap<String, Long>> entry = itr2.next();
			HashMap<String, Long> data = entry.getValue();

			content = String.format("%d\t%d\t%d\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\n",
					entry.getKey(),
					data.get("arrivalTime"),
					data.get("failures"),
					msToSec(data.get("makespan")),
					msToSec(data.get("waitTime")),
					msToSec(data.get("runningTime")),
					msToSec(data.get("uploadTime")),
					msToSec(data.get("tarballExtractionTime")),
					msToSec(data.get("executionTime")),
					msToSec(data.get("downloadTime"))
			);

			writer.write(content);
		}
		
		/* detail VM performance */
		writer.write("\n====================\nDetail VM Performance\n====================\n");
		writer.write("#id startTime deadTime lifeTime bootingTime preparationTime idleTime busyTime\n");
		itr3 = this.vmMap.entrySet().iterator();
		while (itr3.hasNext()) {
			Entry<Integer, HashMap<String, Long>> entry = itr3.next();
			HashMap<String, Long> data = entry.getValue();

			content = String.format(
					"%d\t%d\t%d\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\n",
					entry.getKey(), 
					data.get("startTime"), 
					data.get("deadTime"),
					msToSec(data.get("lifeTime")),
					msToSec(data.get("bootingTime")),
					msToSec(data.get("preperationTime")),
					msToSec(data.get("idleTime")),
					msToSec(data.get("busyTime")));

			writer.write(content);
		}
		
		/* flush and close */
		writer.flush();
		writer.close();
	}
	
	private Double msToSec(Long ms) {
		Double sec = null;
		if (ms != null) {
			sec = (double) ms / 1000;
		}
		return sec;
	}
	
	private Long[] createStats() {
		Long[] metric = new Long[3];
		metric[0] = Long.MAX_VALUE;
		metric[1] = 0L;
		metric[2] = Long.MIN_VALUE;
		return metric;
	}

	private void updateStats(Long[] metric, Long value) {
		if (value != null) {
			metric[0] = metric[0] < value ? metric[0] : value;
			metric[1] += value;
			metric[2] = metric[2] > value ? metric[2] : value;
		}
	}

	private String formatStats(Long[] metric, Long total) {
		return String.format("[%.3f %.3f %.3f]",
				msToSec(metric[0]),
				msToSec(metric[1]) / total,
				msToSec(metric[2]));
	}
	
}
